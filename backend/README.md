# Backend

Le backend est basé sur les frameworks [Symfony](https://symfony.com/doc/current/index.html)
et [API Platform](https://api-platform.com/).

![Logo Symfony](https://framapic.org/UoMC7jvG4IIc/iy2TOOyqWNjY.png)
![Logo API Platform](https://framapic.org/34yPx1Y56SW7/rqvjAwyChY7T.png)

## Normes de codage

### Namespaces

Chaque groupe de fonctionnalités possède son propre namespace (ex: `Author`, `User`, `PublishingHouse`, ...).  
Pour éviter les dépendances cyclques, deux namespace ne s'utilisent pas mutuellement.

### CrudService

Pour simplifier l'instanciation des services du métier relatifs aux entités, celles-ci implémentent toutes
`App\Domain\Entity\EntityInterface`.  
Si elles utilisent les annotations de contraintes pour la validation de Symfony, elles implémetent également 
`App\Domain\Validation\Validable`.

Les services relatifs aux entités héritent de l'`App\Domain\Service\AbstractCrudService`.

### Architecture hexagonale

L'application utilise des principes de l'architecture hexagonale.
  
* [Wikipédia](https://fr.wikipedia.org/wiki/Architecture_hexagonale_(logiciel)
* [Un bon article en fraçais sur le sujet](https://blog.engineering.publicissapient.fr/2016/03/16/perennisez-votre-metier-avec-larchitecture-hexagonale/)

Les sources de l'application sont donc divisées en trois couches/packages : 
**Application**, **Domain** et **Infrastructure**.

#### Entorses à l'architecture

Normalement, le domaine n'a pas connaissance des deux autres couches, ni de toutes autres lib.

Pour simplifier (grandement) l'utilisation de Doctrine et surtout d'API Platform, pas mal d'entorses à l'architecture
sont autorisées (dans un premier temps ?). 

Les entités sont les principales à ne pas tenier compte de ces restrictions. Elles portent des annotations 
pour ces deux frameworks (ex: `@ApiResource`, `@Assert` et `@ORM\Entity`) et utilisent les collections du premier.  
De plus, las annotations d'API PLatform poussent à référencer les contrôleurs de la couche Application dans les entités.

De même, pour simplifier le formattage des exceptions via API Platform, toutes les exceptions du domaine 
doivent implémenter son `ApiPlatform\Core\ExceptionExceptionInterface`. 
Pour limiter cette référence elles implémentent à la place `App\Domain\Exception\ExceptionInterface`, 
qui contiendra l'unique référence vers API Platform.

De la même manière, l'interface `ObjectRepository` de doctrine étant satisfaisante. 
Tous les repositorys l'implémentent donc indirectement, via l'`App\Domain\Repository\EntityRepositoryInterface`.
C'est elle qui contient l'unique référence vers Doctrine.

Même principe pour les évènements, qui héritent de `App\Domain\Event\AbstractEvent`, qui
hérite lui-même de `Symfony\Contracts\EventDispatcher\Event`.
Les écouteurs d'évènements implémentent quant à eux `App\Domain\Event\EventSubscriberInterface`, plutôt que directement
`Symfony\Component\EventDispatcher\EventSubscriberInterface`

#### Couche Application

Elle contient tous les points d'entrées de l'application : les **contrôleurs** et les **commandes Symfony**.  s
Les classes en rapport avec API Platform s'y trouve également (ex: documentation pour Swagger, serializers, ...).

#### Couche Domain

On y trouve tout le code métier :

* les entités
* les interfaces pour les repository
* les services du métier

#### Couche Infrastructure

Elle contient tout ce qui concerent les traitements bas niveaux, l'accès aux données, au système de fichier, ...  
Les repositorys implémentant les interfaces de la couche Domaine sont placés ici. 

On y trouve également :
* tout ce qui concerne la lib de conversion de document Pandoc
* ce qui concerne le parsage de document
* les migrations pour Doctrine
* les fixtures
