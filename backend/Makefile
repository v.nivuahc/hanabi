.DEFAULT_GOAL := help
help:
	@grep -E '(^[1-9a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help

## -------------
## Images
## -------------

## Base

pull-base:
	docker login -u v.nivuahc registry.gitlab.com
	docker pull registry.gitlab.com/v.nivuahc/hanabi/backend/base:latest

build-base-and-push:
	./docker/build.sh base --push

## App

pull-dev:
	docker login -u v.nivuahc registry.gitlab.com
	docker pull registry.gitlab.com/v.nivuahc/hanabi/backend/dev:latest

build-dev-and-push:
	./docker/build.sh dev --push

## Tests

pull-test:
	docker login -u v.nivuahc registry.gitlab.com
	docker pull registry.gitlab.com/v.nivuahc/hanabi/backend/test:latest

build-test-and-push:
	./docker/build.sh test --push

## All

pull-all:
	docker login -u v.nivuahc registry.gitlab.com
	docker pull registry.gitlab.com/v.nivuahc/hanabi/backend/base:latest
	docker pull registry.gitlab.com/v.nivuahc/hanabi/backend/dev:latest
	docker pull registry.gitlab.com/v.nivuahc/hanabi/backend/test:latest

build-all-and-push:
	./docker/build.sh base --push
	./docker/build.sh dev --push
	./docker/build.sh test --push

## -------------
## Tools
## -------------

## App

exec:
	docker-compose -p hanabi -f ./docker/docker-compose.yml up -d
	docker-compose -p hanabi -f ./docker/docker-compose.yml exec php \
	$(CMD)

up:
	docker-compose -p hanabi -f ./docker/docker-compose.yml up

down:
	docker-compose -p hanabi -f ./docker/docker-compose.yml down

## PHPStan / PHPCS

phpcs:
	docker run -i --rm -v $(shell pwd):/srv/sources:ro registry.gitlab.com/v.nivuahc/docker-tools/phpcs:latest

phpstan:
	docker-compose -p hanabi-phpstan -f ./docker/docker-compose-phpstan.yml run --rm php_phpstan  \
	php -d memory_limit=-1 vendor/bin/phpstan.phar analyse -l 8 src
	|| docker-compose -p hanabi-phpstan -f ./docker/docker-compose-phpstan.yml down
	docker-compose -p hanabi-phpstan -f ./docker/docker-compose-phpstan.yml down

## Tests

test:
	docker-compose -p hanabi-test -f ./docker/docker-compose-test.yml run --rm php_test \
	./vendor/bin/simple-phpunit \
	|| docker-compose -p hanabi-test -f ./docker/docker-compose-test.yml down
	docker-compose -p hanabi-test -f ./docker/docker-compose-test.yml down

test-coverage:
	docker-compose -p hanabi-test -f ./docker/docker-compose-test.yml run --rm php_test \
	./vendor/bin/simple-phpunit --coverage-html ./tests-coverage \
	|| docker-compose -p hanabi-test -f ./docker/docker-compose-test.yml down
	docker-compose -p hanabi-test -f ./docker/docker-compose-test.yml down

test-one: # ex: make test-one TEST="Domain/Author/Service/AuthorServiceTest"
	docker-compose -p hanabi-test -f ./docker/docker-compose-test.yml run --rm php_test \
	./vendor/bin/simple-phpunit \
	/srv/app/tests/${TEST} \
	|| docker-compose -p hanabi-test -f ./docker/docker-compose-test.yml down
	docker-compose -p hanabi-test -f ./docker/docker-compose-test.yml down


## Package

package:
	mkdir -p ci_build
	docker run -i --rm -v $(shell pwd):/srv/sources:rw registry.gitlab.com/v.nivuahc/docker-tools/package:latest
