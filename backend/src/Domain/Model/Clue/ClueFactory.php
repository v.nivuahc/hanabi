<?php

namespace App\Domain\Model\Clue;

use App\Domain\Model\Card\Color;
use App\Domain\Model\Card\Value;

class ClueFactory
{
    public static function getClue(string $clue): ClueInterface
    {
        return is_numeric($clue) ? new ValueClue(new Value($clue)):new ColorClue(new Color($clue));
    }
}
