<?php

namespace App\Domain\Model\Clue;

use App\Domain\Model\Card\Color;

class ColorClue implements ClueInterface
{
    private ?int $id = null;
    private Color $color;

    public function __toString(): string
    {
        return $this->getColor()->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function __construct(Color $color)
    {
        $this->color = $color;
    }

    public function getColor(): Color
    {
        return $this->color;
    }
}
