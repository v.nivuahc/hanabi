<?php

namespace App\Domain\Model\Clue;

use App\Domain\Model\Card\Value;

class ValueClue implements ClueInterface
{
    private ?int $id = null;
    private Value $value;

    public function __toString(): string
    {
        return $this->getValue();
    }

    public function __construct(Value $value)
    {
        $this->value = $value;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getValue(): string
    {
        return $this->value->getValue();
    }
}
