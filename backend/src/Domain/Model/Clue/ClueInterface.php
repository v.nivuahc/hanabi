<?php

namespace App\Domain\Model\Clue;

interface ClueInterface
{
    public function __toString(): string;
}
