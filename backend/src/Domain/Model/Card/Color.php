<?php

namespace App\Domain\Model\Card;

class Color
{
    public const BLUE = 'blue';
    public const GREEN = 'green';
    public const RED = 'red';
    public const WHITE = 'white';
    public const YELLOW = 'yellow';
    public const MULTI = 'multi';

    public const NAMES = [
        self::BLUE,
        self::GREEN,
        self::RED,
        self::WHITE,
        self::YELLOW,
        self::MULTI,
    ];

    private string $name;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        if (!\in_array($name, static::NAMES)) {
            throw new \InvalidArgumentException(sprintf(
                'Couleur "%s" invalide. Couleurs possibles: %s',
                $name,
                implode(', ', static::NAMES)
            ));
        }

        $this->name = $name;
    }

    /**
     * Compare la couleur avec celle en argument.
     *
     * @param Color $color
     *
     * @return int
     */
    public function compareTo(Color $color): int
    {
        return strcmp($this->getName(), $color->getName());
    }

    public function getName(): string
    {
        return $this->name;
    }
}
