<?php

namespace App\Domain\Model\Card;

class Value
{
    public const VALUES = ['0', '1', '2', '3', '4', '5'];

    private string $value;

    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        if (!\in_array($value, static::VALUES, true)) {
            throw new \InvalidArgumentException(sprintf(
                'Valeur "%s" invalide. Valeurs possibles: %s',
                $value,
                implode(', ', static::VALUES)
            ));
        }

        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
