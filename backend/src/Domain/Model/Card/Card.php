<?php

namespace App\Domain\Model\Card;

use App\Domain\Model\Clue\ClueInterface;
use App\Domain\Model\Clue\ColorClue;
use App\Domain\Model\Clue\ValueClue;

class Card
{
    private ?int $id = null;
    private int $index = 0;
    private CardType $type;
    private ?ColorClue $colorClue = null;
    private ?ValueClue $valueClue = null;

    /**
     * @param CardType $type
     */
    public function __construct(CardType $type)
    {
        $this->type = $type;
    }

    /**
     * Retourne le nom de la carte avec l'initiale de la couleur et sa valeur concaténées (ex : G2, R5, M1, ...).
     */
    public function getName(): string
    {
        return $this->getType()->getName();
    }

    /**
     * Compare la carte avec celle en argument, en se basant sur leurs valeurs.
     *
     * @param Card $card
     *
     * @return int
     */
    public function compareTo(Card $card): int
    {
        return (int) $card->getValue() <=> (int) $this->getValue();
    }

    /**
     * Retourne la valeur de la carte.
     */
    public function getValue(): string
    {
        return $this->getType()->getValue()->getValue();
    }

    /**
     * Retourne la couleur de la carte.
     */
    public function getColor(): Color
    {
        return $this->getType()->getColor();
    }

    /**
     * Ajoute l'indice à la carte, si elle est concernée.
     *
     * @param ClueInterface $clue
     * @param bool          $mergeColorClues
     */
    public function clue(ClueInterface $clue, bool $mergeColorClues): void
    {
        if ($clue instanceof ValueClue && $clue->getValue() === $this->getValue()) {
            $this->setValueClue($clue);
        }

        if ($clue instanceof ColorClue) {
            if ($clue->getColor()->compareTo($this->getColor()) === 0) {
                $this->setColorClue($clue);
            } elseif ($mergeColorClues && $this->getColor()->getName() === Color::MULTI) {
                if ($this->getColorClue() === null) {
                    $this->setColorClue($clue);
                } else {
                    $this->setColorClue(new ColorClue(new Color(Color::MULTI)));
                }
            }
        }
    }

    /**
     * Supprime les indices sur la carte.
     */
    public function removeClues(): self
    {
        $this->setColorClue(null);
        $this->setValueClue(null);

        return $this;
    }

    public function getIndex(): int
    {
        return $this->index;
    }

    public function setIndex(int $index): self
    {
        $this->index = $index;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getType(): CardType
    {
        return $this->type;
    }

    public function getColorClue(): ?ColorClue
    {
        return $this->colorClue;
    }

    public function setColorClue(?ColorClue $colorClue): self
    {
        $this->colorClue = $colorClue;

        return $this;
    }

    public function getValueClue(): ?ValueClue
    {
        return $this->valueClue;
    }

    public function setValueClue(?ValueClue $valueClue): self
    {
        $this->valueClue = $valueClue;

        return $this;
    }
}
