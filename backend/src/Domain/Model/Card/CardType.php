<?php

namespace App\Domain\Model\Card;

class CardType
{
    private ?int $id = null;
    private Value $value;
    private Color $color;

    public function __construct(Value $value, Color $color)
    {
        $this->value = $value;
        $this->color = $color;
    }

    /**
     * Retourne le nom de la carte avec le nom court de la couleur et sa valeur concaténées (ex : G2, R5, M1, ...).
     */
    public function getName(): string
    {
        return strtoupper($this->getColor()->getName()[0]).$this->getValue()->getValue();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getValue(): Value
    {
        return $this->value;
    }

    public function getColor(): Color
    {
        return $this->color;
    }
}
