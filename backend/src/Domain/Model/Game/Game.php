<?php

namespace App\Domain\Model\Game;

use App\Domain\Collection\Deck;
use App\Domain\Collection\DeckInterface;
use App\Domain\Collection\ListOfPlayers;
use App\Domain\Collection\MapOfCardsByColor;
use App\Domain\Collection\MapOfCardsByColorFactory;
use App\Domain\Exception\DefeatException;
use App\Domain\Model\Game\Config\GameConfig;
use App\Domain\Model\History\History;
use App\Domain\Model\History\Message;
use App\Domain\Model\Player;

/**
 * Une partie.
 */
class Game
{
    public const STATUS_RUNNING = 'running';
    public const STATUS_WON = 'won';
    public const STATUS_LOST = 'lost';
    public const STATUSES = [
        self::STATUS_RUNNING,
        self::STATUS_WON,
        self::STATUS_LOST,
    ];

    private ?int $id = null;
    private GameConfig $config;
    private DeckInterface $deck;
    private MapOfCardsByColor $discard;
    private MapOfCardsByColor $playedCards;
    private int $nbOfRemainingClueToken;
    private int $nbOfRemainingBombsBeforeLose;
    private ListOfPlayers $players;
    private History $history;
    private string $status;

    /**
     * @param GameConfig    $config
     * @param ListOfPlayers $players
     */
    public function __construct(
        GameConfig $config,
        ListOfPlayers $players
    ) {
        $this->config = $config;

        $this->deck = Deck::init($this->config->getCardRepartitionByColor());
        $this->discard = MapOfCardsByColorFactory::initMapOfSortedCardLists();
        $this->playedCards = MapOfCardsByColorFactory::initMapOfSuccessiveCardLists();

        $this->nbOfRemainingClueToken = $this->getNbOfClueTokens();
        $this->nbOfRemainingBombsBeforeLose = $this->getNbOfAcceptableBombs();

        $this->players = $players;
        $this->history = new History();
        $this->status = static::STATUS_RUNNING;
    }

    /**
     * Retourne le score maximal possible pour cette partie.
     *
     * @return int
     */
    public function getMaxScore(): int
    {
        return 5 * \count($this->getConfig()->getColorNames());
    }

    /**
     * Retourne le joueur actif.
     *
     * @return Player
     */
    public function getActivePlayer(): Player
    {
        return $this->players->getActive();
    }

    /**
     * Retourne le nombre total de jetons indice pour cette partie.
     *
     * @return int
     */
    public function getNbOfClueTokens(): int
    {
        return $this->config->getNbOfClueTokens();
    }

    /**
     * Incrémente le nombre de jetons indice disponibles.
     */
    public function incrementNbOfRemainingClueToken(): void
    {
        $this->nbOfRemainingClueToken++;
    }

    /**
     * Décrémente le nombre de jetons indice disponibles.
     */
    public function decrementNbOfRemainingClueToken(): void
    {
        $this->nbOfRemainingClueToken--;
    }

    /**
     * Retourne le nombre de bombes autorisées pour cette partie.
     *
     * @return int
     */
    public function getNbOfAcceptableBombs(): int
    {
        return $this->config->getNbOfAcceptableBombs();
    }

    /**
     * Décrémente le nombre de bombes acceptables restantes avant la défaite.
     *
     * @throws DefeatException
     */
    public function decrementNbOfRemainingBombsBeforeLose(): void
    {
        if ($this->getNbOfRemainingBombsBeforeLose() === 0) {
            throw new DefeatException('Nombre de bombes maximum atteint.');
        }

        $this->nbOfRemainingBombsBeforeLose--;
    }

    /**
     * Retourne si la pioche est vide.
     *
     * @return bool
     */
    public function isDeckEmpty(): bool
    {
        return $this->getDeck()->count() === 0;
    }

    /**
     * Retourne si le nombre de bombes a dépassé la limite autorisée.
     *
     * @return bool
     */
    public function isMaxNbOfBombExceeded(): bool
    {
        return $this->getNbOfRemainingBombsBeforeLose() > $this->getConfig()->getNbOfAcceptableBombs();
    }

    /**
     * Retourne si le nombre de jetons indice disponibles maximum est atteint.
     *
     * @return bool
     */
    public function isMaxNbOfAvailableClueTokensReached(): bool
    {
        return $this->getNbOfRemainingClueToken() === $this->getNbOfClueTokens();
    }

    /**
     * Retourne s'il reste des jetons indice.
     *
     * @return bool
     */
    public function isThereAnyClueTokenAvailable(): bool
    {
        return $this->getNbOfClueTokens() > 0;
    }

    /**
     * Ajoute un message à l'historique.
     *
     * @param Message $message
     *
     * @return Game
     */
    public function addMessage(Message $message): self
    {
        $this->getHistory()->addMessage($message);

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getConfig(): GameConfig
    {
        return $this->config;
    }

    public function getDeck(): DeckInterface
    {
        return $this->deck;
    }

    public function setDeck(Deck $deck): self
    {
        $this->deck = $deck;

        return $this;
    }

    public function getDiscard(): MapOfCardsByColor
    {
        return $this->discard;
    }

    public function setDiscard(MapOfCardsByColor $discard): self
    {
        $this->discard = $discard;

        return $this;
    }

    public function getPlayedCards(): MapOfCardsByColor
    {
        return $this->playedCards;
    }

    public function setPlayedCards(MapOfCardsByColor $playedCards): self
    {
        $this->playedCards = $playedCards;

        return $this;
    }

    public function getNbOfRemainingClueToken(): int
    {
        return $this->nbOfRemainingClueToken;
    }

    public function getNbOfRemainingBombsBeforeLose(): int
    {
        return $this->nbOfRemainingBombsBeforeLose;
    }

    public function getPlayers(): ListOfPlayers
    {
        return $this->players;
    }

    public function getHistory(): History
    {
        return $this->history;
    }

    public function setHistory(History $history): self
    {
        $this->history = $history;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
