<?php

namespace App\Domain\Model\Game;

use App\Domain\Collection\ListOfPlayers;
use App\Domain\Model\Game\Config\GameConfig;
use App\Domain\Model\Game\Config\GameConfigFactory;
use App\Domain\Model\Player;

class GameFactory
{
    public const DEFAULT_NB_OF_PLAYERS = 2;
    public const HAND_SIZE_BY_NB_OF_PLAYERS = [
        2 => 5,
        3 => 5,
        4 => 4,
        5 => 4,
    ];

    /**
     * Initie une nouvelle partie dans le mode et pour le nombre de joueurs spécifiés.
     *
     * @param string $mode
     * @param int    $nbOfPlayers
     *
     * @return Game
     */
    public function initGame(string $mode, int $nbOfPlayers): Game
    {
        $game = new Game(
            GameConfigFactory::initGameConfig($mode),
            new ListOfPlayers(static::getPlayers($nbOfPlayers))
        );

        $game->getDeck()->shuffle();

        return $this->initHands($game);
    }

    /**
     * Initie une nouvelle partie correspondant à la config et aux joueurs en argument.
     *
     * @param Player[]|array $players
     * @param GameConfig     $config
     *
     * @return Game
     */
    public function loadGame(array $players, GameConfig $config): Game
    {
        return new Game($config, new ListOfPlayers($players));
    }

    /**
     * @param int $nbOfPlayers
     *
     * @return Player[]|array
     */
    private static function getPlayers(int $nbOfPlayers): array
    {
        $players = [];
        for ($i = 1; $i <= $nbOfPlayers; $i++) {
            $players[] = new Player('Joueur '.$i);
        }

        return $players;
    }

    /**
     * Initialise la main des joueurs.
     *
     * @param Game $game
     *
     * @return Game
     */
    private function initHands(Game $game): Game
    {
        $nbOfPlayers = $game->getPlayers()->count();
        $handSize = static::HAND_SIZE_BY_NB_OF_PLAYERS[$nbOfPlayers];

        for ($i = 0; $i < $handSize * $nbOfPlayers; $i++) {
            $card = $game->getDeck()->removeFirst();
            $game->getPlayers()->getPlayers()[$i % $nbOfPlayers]->draw($card);
        }

        return $game;
    }
}
