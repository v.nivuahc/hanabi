<?php

namespace App\Domain\Model\Game\Config;

/**
 * Configuration d'une partie en mode normal.
 */
class NormalConfig extends GameConfig
{
    public const MODE = 'normal';

    public const CARD_REPARTITION_BY_COLOR = [
        'blue' => self::DEFAULT_CARD_REPARTITION,
        'green' => self::DEFAULT_CARD_REPARTITION,
        'red' => self::DEFAULT_CARD_REPARTITION,
        'white' => self::DEFAULT_CARD_REPARTITION,
        'yellow' => self::DEFAULT_CARD_REPARTITION,
    ];

    /**
     * Retourne une instance de cette configuration.
     *
     * @return NormalConfig
     */
    public static function instance(): self
    {
        return parent::initConfig(
            static::CARD_REPARTITION_BY_COLOR,
            static::MODE,
            static::NB_OF_CLUE_TOKENS,
            static::NB_OF_ACCEPTABLE_BOMBS,
            static::SHOW_MULTI_CLUE
        );
    }
}
