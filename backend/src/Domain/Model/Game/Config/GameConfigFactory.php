<?php

namespace App\Domain\Model\Game\Config;

class GameConfigFactory
{
    /**
     * Initie une nouvelle configuration de partie dans le mode spécifié.
     *
     * @param string $mode
     *
     * @return GameConfig
     */
    public static function initGameConfig(string $mode): GameConfig
    {
        if ($mode === NormalConfig::MODE) {
            return NormalConfig::instance();
        }
        if ($mode === MoreDifficultConfig::MODE) {
            return MoreDifficultConfig::instance();
        }
        if ($mode === HardConfig::MODE) {
            return HardConfig::instance();
        }
        if ($mode === VeryHardConfig::MODE) {
            return VeryHardConfig::instance();
        }

        throw new \InvalidArgumentException(sprintf('Mode "%s" inconnu', $mode));
    }
}
