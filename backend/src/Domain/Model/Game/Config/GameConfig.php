<?php

namespace App\Domain\Model\Game\Config;

use App\Domain\Model\Card\Color;

/**
 * Configuration de partie.
 */
abstract class GameConfig
{
    public const DEFAULT_CARD_REPARTITION = [1, 1, 1, 2, 2, 3, 3, 4, 4, 5];
    public const UNIQUE_CARD_REPARTITION = [1, 2, 3, 4, 5];

    public const NB_OF_CLUE_TOKENS = 8;
    public const NB_OF_ACCEPTABLE_BOMBS = 2;
    public const SHOW_MULTI_CLUE = true;
    public const CARD_REPARTITION_BY_COLOR = [];

    protected ?int $id;
    protected string $mode;
    /** @var int[][]|array */
    protected array $cardRepartitionByColor = [];
    protected int $nbOfClueTokens = 0;
    protected int $nbOfAcceptableBombs = 0;
    protected bool $showMultiClue = true;

    final public function __construct()
    {
    }

    /**
     * Initialise les cartes disponibles, le nombre de jetons indice et celui de bombes autorisées.
     *
     * @param int[][]|array $cardRepartitionByColor
     * @param string        $mode
     * @param int           $nbOfClueTokens
     * @param int           $nbOfAcceptableBombs
     * @param bool          $showMultiClue
     *
     * @return GameConfig
     */
    protected static function initConfig(
        array $cardRepartitionByColor,
        string $mode,
        int $nbOfClueTokens,
        int $nbOfAcceptableBombs,
        bool $showMultiClue
    ): self {
        $instance = new static();

        $instance->cardRepartitionByColor = $cardRepartitionByColor;
        $instance->mode = $mode;
        $instance->nbOfClueTokens = $nbOfClueTokens;
        $instance->nbOfAcceptableBombs = $nbOfAcceptableBombs;
        $instance->showMultiClue = $showMultiClue;

        return $instance;
    }

    /**
     * Retourne la liste des noms des couleurs.
     *
     * @return string[]|array
     */
    public function getColorNames(): array
    {
        return array_keys($this->cardRepartitionByColor);
    }

    /**
     * Retourne la liste des couleurs.
     *
     * @return Color[]|array
     */
    public function getColors(): array
    {
        $colors = [];
        foreach ($this->getColorNames() as $colorName) {
            $colors[] = new Color($colorName);
        }

        return $colors;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return array|int[][]
     */
    public function getCardRepartitionByColor(): array
    {
        return $this->cardRepartitionByColor;
    }

    /**
     * @param int[][]|array $cardRepartitionByColor
     *
     * @return GameConfig
     */
    public function setCardRepartitionByColor(array $cardRepartitionByColor): self
    {
        $this->cardRepartitionByColor = $cardRepartitionByColor;

        return $this;
    }

    public function getMode(): string
    {
        return $this->mode;
    }

    public function setMode(string $mode): self
    {
        $this->mode = $mode;

        return $this;
    }

    public function getNbOfClueTokens(): int
    {
        return $this->nbOfClueTokens;
    }

    public function setNbOfClueTokens(int $nbOfClueTokens): self
    {
        $this->nbOfClueTokens = $nbOfClueTokens;

        return $this;
    }

    public function getNbOfAcceptableBombs(): int
    {
        return $this->nbOfAcceptableBombs;
    }

    public function setNbOfAcceptableBombs(int $nbOfAcceptableBombs): self
    {
        $this->nbOfAcceptableBombs = $nbOfAcceptableBombs;

        return $this;
    }

    public function isShowMultiClue(): bool
    {
        return $this->showMultiClue;
    }

    public function setShowMultiClue(bool $showMultiClue): self
    {
        $this->showMultiClue = $showMultiClue;

        return $this;
    }
}
