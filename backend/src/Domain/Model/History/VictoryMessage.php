<?php

namespace App\Domain\Model\History;

use App\Domain\Model\Player;

class VictoryMessage extends Message
{
    /** @var string[] Qualités possibles de la prestation */
    public const PRESTATIONS = [
        'horrible, huées de la foule...',
        'médiocre, à peine quelques applaudissements',
        'honorable, mais ne restera pas dans les mémoires...',
        'excellente, ravit la foule.',
        'extraordinaire, restera gravée dans les mémoires !',
        'légendaire, petits et grands restent sans voix, des étoiles dans les yeux !',
    ];

    private int $score;
    private int $maxScore;

    public function __construct(int $score, int $maxScore)
    {
        parent::__construct(Message::TYPE_INFO, new \DateTime());
        $this->score = $score;
        $this->maxScore = $maxScore;
    }

    /**
     * @inheritDoc
     */
    public function getContent(?Player $currentPlayer): string
    {
        return sprintf(
            'Partie gagnée ! Score de {score}%s{/score}. Prestation %s',
            $this->getScore(),
            $this->getPrestation(),
        );
    }

    public function getScore(): int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getMaxScore(): int
    {
        return $this->maxScore;
    }

    public function setMaxScore(int $maxScore): self
    {
        $this->maxScore = $maxScore;

        return $this;
    }

    /**
     * Retourne le message de qualité de prestation en fonction du score.
     *
     * @return string
     */
    private function getPrestation(): string
    {
        $nbOfPrestationGrades = \count(static::PRESTATIONS);

        if ($this->getScore() === $this->getMaxScore()) {
            return static::PRESTATIONS[$nbOfPrestationGrades - 1];
        }

        for ($i = 1; $i < $nbOfPrestationGrades; $i++) {
            if ($this->getScore() > $this->getMaxScore() - (5 * $i)) {
                return static::PRESTATIONS[$nbOfPrestationGrades - ($i + 1)];
            }
        }

        throw new \InvalidArgumentException(sprintf(
            'Pas de message de prestation pour le score "%s"',
            $this->getScore()
        ));
    }
}
