<?php

namespace App\Domain\Model\History;

use App\Domain\Model\Player;

class ErrorMessage extends Message
{
    private string $content;

    public function __construct(string $content)
    {
        parent::__construct(Message::TYPE_ERROR, new \DateTime());

        $this->content = $content;
    }

    /**
     * @inheritDoc
     */
    public function getContent(?Player $currentPlayer): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }
}
