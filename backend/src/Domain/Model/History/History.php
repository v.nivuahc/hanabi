<?php

namespace App\Domain\Model\History;

class History
{
    /** @var Message[]|array */
    private array $messages = [];

    /**
     * Ajoute un message à l'historique.
     *
     * @param Message $message
     *
     * @return $this
     */
    public function addMessage(Message $message): self
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * @return Message[]|array
     */
    public function getMessages(): array
    {
        return $this->messages;
    }
}
