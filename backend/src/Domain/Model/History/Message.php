<?php

namespace App\Domain\Model\History;

use App\Domain\Model\Player;

abstract class Message
{
    public const TYPE_ERROR = 'error';
    public const TYPE_INFO = 'info';

    private ?int $id = null;
    private string $type;
    private \DateTime $createdAt;

    /**
     * @param string    $type
     * @param \DateTime $createdAt
     */
    public function __construct(string $type, \DateTime $createdAt)
    {
        if (!\in_array($type, [static::TYPE_ERROR, static::TYPE_INFO])) {
            throw new \InvalidArgumentException(
                sprintf('Type "%s" inconnu', $type)
            );
        }

        $this->type = $type;
        $this->createdAt = $createdAt;
    }

    /**
     * Retourne le contenu du message, pour le joueur courant.
     *
     * @param Player|null $currentPlayer
     *
     * @return string
     */
    abstract public function getContent(?Player $currentPlayer): string;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
