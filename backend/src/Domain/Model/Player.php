<?php

namespace App\Domain\Model;

use App\Domain\Model\Card\Card;
use App\Domain\Collection\ListOfCards;
use App\Domain\Collection\ListOfCardsInterface;

class Player
{
    private ?int $id = null;
    private string $name;
    private ListOfCardsInterface $hand;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->hand = new ListOfCards();
    }

    /**
     * Ajoute la carte à la main du joueur.
     *
     * @param Card $card
     *
     * @return $this
     */
    public function draw(Card $card): self
    {
        $this->hand->addCard($card);

        return $this;
    }

    /**
     * Retire la carte de la main du joueur.
     *
     * @param Card $card
     *
     * @return $this
     */
    public function removeFromHand(Card $card): self
    {
        $this->hand->removeCard($card);
        $card->removeClues();

        return $this;
    }

    /**
     * Remplace la main du joueur par les cartes en argument.
     *
     * @param Card[]|array $cards
     *
     * @return $this
     */
    public function initHand(array $cards): self
    {
        $this->hand = new ListOfCards();
        foreach ($cards as $card) {
            $this->hand->addCard($card);
        }

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getHand(): ListOfCardsInterface
    {
        return $this->hand;
    }
}
