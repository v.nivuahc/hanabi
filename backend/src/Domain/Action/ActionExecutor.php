<?php

namespace App\Domain\Action;

use App\Domain\Model\Clue\ClueFactory;
use App\Domain\Model\Game\Game;
use App\Domain\Model\Player;
use App\Infrastructure\Persistence\Persister;

class ActionExecutor
{
    private ActionFactory $actionFactory;
    private Persister $persister;

    public function __construct(
        ActionFactory $actionFactory,
        Persister $persister
    ) {
        $this->actionFactory = $actionFactory;
        $this->persister = $persister;
    }

    /**
     * Effectue l'action du type souhaité avec les arguments nécessaires.
     *
     * Sauvegarde l'état du jeu.
     *
     * @param string        $type
     * @param Game          $game
     * @param Player        $actor
     * @param mixed[]|array $args
     */
    public function execute(string $type, Game $game, Player $actor, array $args): void
    {
        $action = $this->actionFactory->getAction($type, $game, $actor);
        $action->execute($this->convertArgs($game, $actor, $args));

        $this->persister->persist($game);
    }

    /**
     * Convertit les arguments en objets.
     *
     * @param Game          $game
     * @param Player        $actor
     * @param mixed[]|array $args
     *
     * @return mixed[]|array
     */
    private function convertArgs(Game $game, Player $actor, array $args): array
    {
        if (!empty($args['target_player'])) {
            $args['target_player'] = $game->getPlayers()->getPlayer($args['target_player']);
        }
        if (!empty($args['card'])) {
            $args['card'] = $actor->getHand()->getCard($args['card']);
        }
        if (!empty($args['clue'])) {
            $args['clue'] = ClueFactory::getClue($args['clue']);
        }

        return $args;
    }
}
