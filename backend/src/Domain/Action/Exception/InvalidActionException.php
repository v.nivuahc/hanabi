<?php

namespace App\Domain\Action\Exception;

use App\Domain\Exception\HanabiExceptionInterface;

class InvalidActionException extends \DomainException implements HanabiExceptionInterface
{

}
