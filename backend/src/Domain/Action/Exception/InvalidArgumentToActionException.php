<?php

namespace App\Domain\Action\Exception;

use App\Domain\Exception\HanabiExceptionInterface;

class InvalidArgumentToActionException extends \DomainException implements HanabiExceptionInterface
{

}
