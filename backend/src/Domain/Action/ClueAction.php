<?php

namespace App\Domain\Action;

use App\Domain\Action\Exception\InvalidActionException;
use App\Domain\Action\Message\ClueMessage;
use App\Domain\Model\Card\Color;
use App\Domain\Model\Clue\ClueInterface;
use App\Domain\Model\Clue\ColorClue;
use App\Domain\Model\Game\Config\VeryHardConfig;
use App\Domain\Model\Game\Game;
use App\Domain\Model\History\ErrorMessage;
use App\Domain\Model\Player;

class ClueAction implements ActionInterface
{
    protected Game $game;
    protected Player $actor;

    public function __construct(Game $game, Player $actor)
    {
        $this->game = $game;
        $this->actor = $actor;
    }

    /**
     * @inheritDoc
     */
    final public function checkArgs(array $args): void
    {
        if (empty($args['clue'])
            || !$args['clue'] instanceof ClueInterface) {
            throw new \InvalidArgumentException('L\'action nécessite un indice en entrée');
        }

        if (empty($args['target_player']) || !$args['target_player'] instanceof Player) {
            throw new \InvalidArgumentException('L\'action nécessite un joueur à qui donner l\'indice en entrée');
        }
    }

    /**
     * @inheritDoc
     */
    final public function execute(array $args): void
    {
        $this->checkArgs($args);
        /** @var ClueInterface $clue */
        $clue = $args['clue'];
        /** @var Player $targetPlayer */
        $targetPlayer = $args['target_player'];

        if ($this->actor !== $this->game->getActivePlayer()) {
            throw new InvalidActionException('Action impossible. C\'est le tour d\'un autre joueur.');
        }
        if ($targetPlayer === $this->actor) {
            throw new InvalidActionException('Action impossible. On ne peut se donner d\'indice à soi-même.');
        }
        if ($this->game->getNbOfRemainingClueToken() < 1) {
            $message = 'Action impossible. Aucun jeton indice disponible.';
            $this->game->addMessage(new ErrorMessage($message));
            throw new InvalidActionException($message);
        }
        if ($this->isMultiClue($clue) && $this->isModeVeryHard($this->game)) {
            throw new InvalidActionException('Action impossible. Indice "multicolore" absent en mode très difficile.');
        }

        // Retrait d'un jeton indice
        $this->game->decrementNbOfRemainingClueToken();

        // Ajout de l'indice aux cartes du joueur concernées
        $mergeColorClues = $this->isModeVeryHard($this->game);
        foreach ($targetPlayer->getHand()->getCards() as $card) {
            $card->clue($clue, $mergeColorClues);
        }
        $this->game->addMessage(new ClueMessage($this->actor, $clue, $targetPlayer));

        // Passage au joueur suivant
        $this->game->getPlayers()->next();
    }

    /**
     * Retourne si l'indice est "multicolore".
     *
     * @param ClueInterface $clue
     *
     * @return bool
     */
    private function isMultiClue(ClueInterface $clue): bool
    {
        return $clue instanceof ColorClue && $clue->getColor()->getName() === Color::MULTI;

    }

    /**
     * Retourne si le jeu est en mode "très difficile".
     *
     * @param Game $game
     *
     * @return bool
     */
    private function isModeVeryHard(Game $game): bool
    {
        return $game->getConfig()->getMode() === VeryHardConfig::MODE;
    }
}
