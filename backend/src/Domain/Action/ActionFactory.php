<?php

namespace App\Domain\Action;

use App\Domain\Action\Exception\InvalidActionException;
use App\Domain\Model\Game\Game;
use App\Domain\Model\Player;

class ActionFactory
{
    public const ACTION_CLUE = 'clue';
    public const ACTION_PLAY = 'play';
    public const ACTION_DISCARD = 'discard';

    /**
     * Retourne une action du type souhaité.
     *
     * @param string $type
     * @param Game   $game
     * @param Player $actor
     *
     * @return ActionInterface
     */
    public function getAction(string $type, Game $game, Player $actor): ActionInterface
    {
        switch ($type) {
            case self::ACTION_CLUE:
                return $this->getClueAction($game, $actor);

            case self::ACTION_PLAY:
                return $this->getPlayCardAction($game, $actor);

            case self::ACTION_DISCARD:
                return $this->getDiscardAction($game, $actor);

            default:
                throw new InvalidActionException(sprintf('Action "%s" inconnue', $type));
        }
    }

    /**
     * Retourne une nouvelle action d'indice.
     *
     * @param Game   $game
     * @param Player $actor
     *
     * @return ActionInterface
     */
    private function getClueAction(Game $game, Player $actor): ActionInterface
    {
        return new ClueAction($game, $actor);
    }

    /**
     * Retourne une nouvelle action pour joueur une carte.
     *
     * @param Game   $game
     * @param Player $actor
     *
     * @return ActionInterface
     */
    private function getPlayCardAction(Game $game, Player $actor): ActionInterface
    {
        return new PlayCardAction($game, $actor);
    }

    /**
     * Retourne une nouvelle action de défausse.
     *
     * @param Game   $game
     * @param Player $actor
     *
     * @return ActionInterface
     */
    private function getDiscardAction(Game $game, Player $actor): ActionInterface
    {
        return new DiscardAction($game, $actor);
    }
}
