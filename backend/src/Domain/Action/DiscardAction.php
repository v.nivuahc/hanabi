<?php

namespace App\Domain\Action;

use App\Domain\Action\Exception\InvalidActionException;
use App\Domain\Action\Message\DiscardCardMessage;
use App\Domain\Action\Message\DrawCardMessage;
use App\Domain\Model\Card\Card;
use App\Domain\Model\Game\Game;
use App\Domain\Model\History\ErrorMessage;
use App\Domain\Model\Player;

class DiscardAction implements ActionInterface
{
    protected Game $game;
    protected Player $actor;

    /**
     * @param Game   $game
     * @param Player $actor
     */
    public function __construct(
        Game $game,
        Player $actor
    ) {
        $this->game = $game;
        $this->actor = $actor;
    }

    /**
     * @inheritDoc
     */
    final public function checkArgs(array $args): void
    {
        if (empty($args['card']) || !$args['card'] instanceof Card) {
            throw new \InvalidArgumentException('L\'action nécessite une carte en entrée');
        }
    }

    /**
     * @inheritDoc
     */
    final public function execute(array $args): void
    {
        $this->checkArgs($args);
        /** @var Card $card */
        $card = $args['card'];

        if ($this->actor !== $this->game->getActivePlayer()) {
            throw new InvalidActionException('Action impossible. C\'est le tour d\'un autre joueur.');
        }
        if (!$this->actor->getHand()->contains($card)) {
            throw new InvalidActionException('Action impossible. La carte n\'est pas en main.');
        }
        if ($this->game->isMaxNbOfAvailableClueTokensReached()) {
            $message = 'Action impossible. Tous les jetons indice sont déjà disponibles.';
            $this->game->addMessage(new ErrorMessage($message));
            throw new InvalidActionException($message);
        }

        // Défausse de la carte
        $this->actor->removeFromHand($card);
        $this->game->getDiscard()->addCard($card);
        $this->game->addMessage(new DiscardCardMessage($this->actor, $card));

        // Gain d'un jeton indice
        $this->game->incrementNbOfRemainingClueToken();

        // Pioche d'une nouvelle carte (si la pioche n'est pas vide)
        $newCard = $this->game->getDeck()->draw();
        if ($newCard !== null) {
            $this->actor->draw($newCard);
            $this->game->addMessage(new DrawCardMessage($this->actor, $newCard));
        }

        // Passage au joueur suivant
        $this->game->getPlayers()->next();

    }
}
