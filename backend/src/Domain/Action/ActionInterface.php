<?php

namespace App\Domain\Action;

interface ActionInterface
{
    /**
     * Vérifie les arguments à passer à l'action.
     *
     * @param mixed[]|array $args
     */
    public function checkArgs(array $args): void;

    /**
     * Exécute l'action.
     *
     * @param mixed[]|array $args
     */
    public function execute(array $args): void;
}
