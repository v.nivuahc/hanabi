<?php

namespace App\Domain\Action\Message;

use App\Domain\Model\Card\Card;
use App\Domain\Model\History\Message;
use App\Domain\Model\Player;

class DrawCardMessage extends CardActionMessage
{
    public function __construct(
        Player $actor,
        Card $card
    ) {
        parent::__construct(
            Message::TYPE_INFO,
            new \DateTime(),
            $actor,
            $card
        );
    }

    /**
     * @inheritDoc
     */
    public function getContent(?Player $currentPlayer): string
    {
        if ($this->isActorCurrentPlayer($currentPlayer)) {
            return '';
        }

        return sprintf(
            '{player}%s{/player} a pioché la carte {card}{%s}%s{/%s}{/card}',
            $this->getActor()->getName(),
            $this->getCard()->getColor()->getName(),
            $this->getCard()->getName(),
            $this->getCard()->getColor()->getName()
        );
    }
}
