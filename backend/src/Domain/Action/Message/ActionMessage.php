<?php

namespace App\Domain\Action\Message;

use App\Domain\Model\History\Message;
use App\Domain\Model\Player;

abstract class ActionMessage extends Message
{
    private Player $actor;

    /**
     * @param string    $type
     * @param \DateTime $createdAt
     * @param Player    $actor
     */
    public function __construct(
        string $type,
        \DateTime $createdAt,
        Player $actor
    ) {
        parent::__construct($type, $createdAt);
        $this->actor = $actor;
    }

    /**
     * Retourne si l'acteur de l'action est le joueur courant.
     *
     * @param Player|null $currentPlayer
     *
     * @return bool
     */
    public function isActorCurrentPlayer(?Player $currentPlayer): bool
    {
        return $currentPlayer !== null && $currentPlayer->getId() === $this->getActor()->getId();
    }

    /**
     * @return Player
     */
    public function getActor(): Player
    {
        return $this->actor;
    }

    /**
     * @param Player $actor
     *
     * @return ActionMessage
     */
    public function setActor(Player $actor): self
    {
        $this->actor = $actor;

        return $this;
    }
}
