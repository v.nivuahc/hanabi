<?php

namespace App\Domain\Action\Message;

use App\Domain\Model\Card\Card;
use App\Domain\Model\Player;

abstract class CardActionMessage extends ActionMessage
{
    private Card $card;

    /**
     * @param string    $type
     * @param \DateTime $createdAt
     * @param Player    $actor
     * @param Card      $card
     */
    public function __construct(
        string $type,
        \DateTime $createdAt,
        Player $actor,
        Card $card
    ) {
        parent::__construct($type, $createdAt, $actor);

        $this->card = $card;
    }

    public function getCard(): Card
    {
        return $this->card;
    }

    public function setCard(Card $card): self
    {
        $this->card = $card;

        return $this;
    }
}
