<?php

namespace App\Domain\Action\Message;

use App\Domain\Model\Card\Card;
use App\Domain\Model\History\Message;
use App\Domain\Model\Player;

class PlayCardMessage extends CardActionMessage
{
    public function __construct(
        Player $actor,
        Card $card,
        bool $success
    ) {
        parent::__construct(
            $success ? Message::TYPE_INFO : Message::TYPE_ERROR,
            new \DateTime(),
            $actor,
            $card
        );
    }

    /**
     * @inheritDoc
     */
    public function getContent(?Player $currentPlayer): string
    {
        $player = $this->isActorCurrentPlayer($currentPlayer)
            ? '{player}Vous{/player} avez'
            : sprintf('{player}%s{/player} a', $this->getActor()->getName());
        $action = $this->getType() === Message::TYPE_INFO ? ' joué' : ' essayé de jouer';

        return sprintf(
            '%s %s la carte {card}{%s}%s{/%s}{/card}',
            $player,
            $action,
            $this->getCard()->getColor()->getName(),
            $this->getCard()->getName(),
            $this->getCard()->getColor()->getName()
        );
    }
}
