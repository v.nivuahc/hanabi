<?php

namespace App\Domain\Action\Message;

use App\Domain\Model\Card\Card;
use App\Domain\Model\History\Message;
use App\Domain\Model\Player;

class DiscardCardMessage extends CardActionMessage
{
    public function __construct(
        Player $actor,
        Card $card
    ) {
        parent::__construct(
            Message::TYPE_INFO,
            new \DateTime(),
            $actor,
            $card
        );
    }

    /**
     * @inheritDoc
     */
    public function getContent(?Player $currentPlayer): string
    {
        $player = $this->isActorCurrentPlayer($currentPlayer)
            ? '{player}Vous{/player} avez'
            : sprintf('{player}%s{/player} a', $this->getActor()->getName());

        return sprintf(
            '%s a défaussé la carte {card}{%s}%s{/%s}{/card}',
            $player,
            $this->getCard()->getColor()->getName(),
            $this->getCard()->getName(),
            $this->getCard()->getColor()->getName()
        );
    }
}
