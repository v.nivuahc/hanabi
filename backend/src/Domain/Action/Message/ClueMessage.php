<?php

namespace App\Domain\Action\Message;

use App\Domain\Model\Clue\ClueInterface;
use App\Domain\Model\Clue\ColorClue;
use App\Domain\Model\History\Message;
use App\Domain\Model\Player;

class ClueMessage extends ActionMessage
{
    private ClueInterface $clue;
    private PLayer $targetPlayer;

    /**
     * @param Player        $actor
     * @param ClueInterface $clue
     * @param Player        $targetPlayer
     */
    public function __construct(
        Player $actor,
        ClueInterface $clue,
        Player $targetPlayer
    ) {
        parent::__construct(Message::TYPE_INFO, new \DateTime(), $actor);
        $this->clue = $clue;
        $this->targetPlayer = $targetPlayer;
    }

    /**
     * @inheritDoc
     */
    public function getContent(?Player $currentPlayer): string
    {
        $clue = $this->getClue()->__toString();
        if ($this->getClue() instanceof ColorClue) {
            $clue = sprintf('{%s}%s{/%s}', $clue, ucfirst($clue), $clue);
        }

        if ($this->isTargetCurrentPlayer($currentPlayer)) {
            return sprintf(
                '{player}%s{/player} {player}vous{/player} a donné l\'indice {clue}%s{/clue}',
                $this->getActor()->getName(),
                $clue,
            );
        }

        $actor = $this->isActorCurrentPlayer($currentPlayer)
            ? '{player}Vous{/player} avez'
            : sprintf('{player}%s{/player} a', $this->getActor()->getName());

        return sprintf(
            '%s donné l\'indice {clue}%s{/clue} à %s',
            $actor,
            $clue,
            $this->getTargetPlayer()->getName(),
        );
    }

    /**
     * Retourne si la cible de l'action est le joueur courant.
     *
     * @param Player|null $currentPlayer
     *
     * @return bool
     */
    public function isTargetCurrentPlayer(?Player $currentPlayer): bool
    {
        return $currentPlayer !== null && $currentPlayer->getId() === $this->getTargetPlayer()->getId();
    }

    public function getClue(): ClueInterface
    {
        return $this->clue;
    }

    public function setClue(ClueInterface $clue): self
    {
        $this->clue = $clue;

        return $this;
    }

    public function getTargetPlayer(): Player
    {
        return $this->targetPlayer;
    }

    public function setTargetPlayer(Player $targetPlayer): self
    {
        $this->targetPlayer = $targetPlayer;

        return $this;
    }
}
