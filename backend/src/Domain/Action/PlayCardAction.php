<?php

namespace App\Domain\Action;

use App\Domain\Action\Exception\InvalidActionException;
use App\Domain\Action\Message\DrawCardMessage;
use App\Domain\Action\Message\PlayCardMessage;
use App\Domain\Exception\DefeatException;
use App\Domain\Exception\InvalidCardAddException;
use App\Domain\Model\Card\Card;
use App\Domain\Model\Game\Game;
use App\Domain\Model\Game\GameFactory;
use App\Domain\Model\History\ErrorMessage;
use App\Domain\Model\History\InfoMessage;
use App\Domain\Model\History\VictoryMessage;
use App\Domain\Model\Player;

class PlayCardAction implements ActionInterface
{
    protected Game $game;
    protected Player $actor;

    /**
     * @param Game   $game
     * @param Player $actor
     */
    public function __construct(Game $game, Player $actor)
    {
        $this->game = $game;
        $this->actor = $actor;
    }

    /**
     * @inheritDoc
     */
    final public function checkArgs(array $args): void
    {
        if (empty($args['card']) || !$args['card'] instanceof Card) {
            throw new \InvalidArgumentException('L\'action nécessite une carte en entrée');
        }
    }

    /**
     * @inheritDoc
     */
    final public function execute(array $args): void
    {
        $this->checkArgs($args);
        /** @var Card $card */
        $card = $args['card'];

        if ($this->actor !== $this->game->getActivePlayer()) {
            throw new InvalidActionException('Action impossible. C\'est le tour d\'un autre joueur.');
        }
        if (!$this->actor->getHand()->contains($card)) {
            throw new InvalidActionException('Action impossible. La carte n\'est pas en main.');
        }

        // Pose de la carte
        $this->actor->removeFromHand($card);
        try {
            $this->game->getPlayedCards()->addCard($card);
            $this->game->addMessage(new PlayCardMessage($this->actor, $card, true));

            // Regain d'un jeton indice si la carte est un 5
            if ($card->getValue() === '5' && $this->canGetOneMoreClue($this->game)) {
                $this->game->incrementNbOfRemainingClueToken();
                $this->game->addMessage(new InfoMessage('5 posé, {clue}1 jeton indice{/clue} récupéré !'));
            }
        } catch (InvalidCardAddException $e) {
            // Pose impossible
            $this->game->addMessage(new PlayCardMessage($this->actor, $card, false));

            // Défausse de la carte
            $this->game->getDiscard()->addCard($card);
            $this->game->addMessage(new ErrorMessage(
                sprintf(
                    '{bomb}+1 Bombe{/bomb} ! La carte {card}{%s}%s{/%s}{card} a été défaussée',
                    $card->getColor()->getName(),
                    $card->getName(),
                    $card->getColor()->getName()
                )
            ));

            // Retrait d'une bombe encore acceptable
            try {
                $this->game->decrementNbOfRemainingBombsBeforeLose();
            } catch (DefeatException $e) {
                $this->game->setStatus(Game::STATUS_LOST);
                $this->game->addMessage(new ErrorMessage(
                        sprintf(
                            'Partie perdue ! {bomb}%s{/bomb} bombes atteintes',
                            $this->game->getConfig()->getNbOfAcceptableBombs() + 1
                        )
                    )
                );
            }
        }

        // Pioche d'une nouvelle carte (si la pioche n'est pas vide)
        $newCard = $this->game->getDeck()->draw();
        if ($newCard !== null) {
            $this->actor->draw($newCard);
            $this->game->addMessage(new DrawCardMessage($this->actor, $newCard));
        } elseif ($this->isGameWon()) {
            // La partie est gagnée
            $this->game->setStatus(Game::STATUS_WON);
            $this->game->addMessage(new VictoryMessage(
                $this->game->getPlayedCards()->count(),
                $this->game->getMaxScore()
            ));
        }

        // Passage au joueur suivant
        $this->game->getPlayers()->next();
    }

    /**
     * Retourne si on peut avoir des jetons indice supplémentaires (= si on est pas déjà au max).
     *
     * @param Game $game
     *
     * @return bool
     */
    private function canGetOneMoreClue(Game $game): bool
    {
        return $game->getNbOfRemainingClueToken() + 1 <= $game->getNbOfClueTokens();
    }

    /**
     * Retourne si la partie est gagnée.
     *
     * La partie est gagnée si tous les joueurs ont une carte de moins qu'au début de partie.
     *
     * @return bool
     */
    private function isGameWon(): bool
    {
        $normalHandSize = GameFactory::HAND_SIZE_BY_NB_OF_PLAYERS[$this->game->getPlayers()->count()];

        foreach ($this->game->getPlayers()->getPlayers() as $player) {
            if ($player->getHand()->count() === $normalHandSize) {
                return false;
            }
        }

        return true;
    }
}
