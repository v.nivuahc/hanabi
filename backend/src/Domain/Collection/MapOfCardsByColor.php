<?php

namespace App\Domain\Collection;

use App\Domain\Exception\InvalidCardAddException;
use App\Domain\Model\Card\Card;
use App\Domain\Model\Card\Color;

class MapOfCardsByColor
{
    /** @var ListOfCardsInterface[]|array */
    protected array $cardsByColor = [];

    /** @var string Classe dont les listes seront des instances */
    private string $listClass;

    /**
     * @param string $listClass
     */
    public function __construct(string $listClass)
    {
        if (!(new $listClass()) instanceof ListOfCardsInterface) {
            throw new \InvalidArgumentException(sprintf(
                'La classe "%s" n\'implémente pas "%s".',
                $listClass,
                ListOfCardsInterface::class
            ));
        }

        $this->listClass = $listClass;
    }

    /**
     * Retourne le nombre de cartes total de la liste.
     */
    public function count(): int
    {
        $count = 0;
        foreach ($this->getCardsByColor() as $cardList) {
            $count += $cardList->count();
        }

        return $count;
    }

    /**
     * Ajoute une couleur pour laquelle lister les cartes.
     *
     * @param Color $color
     */
    public function addColor(Color $color): void
    {
        if (!array_key_exists($color->getName(), $this->cardsByColor)) {
            $this->cardsByColor[$color->getName()] = new $this->listClass();
        }
    }

    /**
     * Ajoute la carte à la liste de sa couleur.
     *
     * @param Card $card
     *
     * @throws InvalidCardAddException
     */
    public function addCard(Card $card): void
    {
        $color = $card->getType()->getColor();

        $this->addColor($color);
        $this->cardsByColor[$color->getName()]->addCard($card);
    }

    /**
     * Retourne la liste des cartes rangées par couleurs.
     *
     * @return ListOfCardsInterface[]|array
     */
    public function getCardsByColor(): array
    {
        return $this->cardsByColor;
    }

    /**
     * Retourne la liste de toutes les cartes défaussées.
     *
     * @return Card[]|array
     */
    public function getAll(): array
    {
        $cards = [];
        foreach ($this->cardsByColor as $cardsOfColor) {
            array_push($cards, ...$cardsOfColor->getCards());
        }

        return $cards;
    }
}
