<?php

namespace App\Domain\Collection;

use App\Domain\Model\Card\Card;

/**
 * Liste de cartes triées par valeurs.
 */
class ListOfSortedCards extends ListOfCards
{
    /**
     * @inheritDoc
     */
    public function addCard(Card $card): void
    {
        parent::addCard($card);
        usort($this->cards, static function (Card $c1, Card $c2) {
            return $c1->compareTo($c2);
        });
    }
}
