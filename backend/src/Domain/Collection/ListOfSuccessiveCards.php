<?php

namespace App\Domain\Collection;

use App\Domain\Exception\InvalidCardAddException;
use App\Domain\Model\Card\Card;

/**
 * Liste de cartes dont les valeurs se suivent.
 *
 * Une carte ne peut y être ajoutée que si sa valeur est 1 ou si elle suit celle de la dernière carte de la liste.
 */
class ListOfSuccessiveCards extends ListOfCards
{
    /**
     * @inheritDoc
     */
    public function addCard(Card $card): void
    {
        $this->checkCanAdd($card);
        parent::addCard($card);
    }

    /**
     * Vérifie que la carte peut bien être ajoutée.
     *
     * Pour cela, la dernière carte de sa couleur doit être celle de valeur juste en dessous.
     *
     * @param Card $card
     *
     * @throws InvalidCardAddException
     */
    private function checkCanAdd(Card $card): void
    {
        $lastValue = 0;
        $cardOfThisColor = $this->getCards();

        if (!empty($cardOfThisColor)) {
            $lastCard = $cardOfThisColor[\count($cardOfThisColor) - 1];
            $lastValue = (int) $lastCard->getValue();
        }

        if ($lastValue !== ((int)$card->getValue()) - 1) {
            throw new InvalidCardAddException(sprintf(
                'Impossible d\'ajouter la carte "%s". La valeur précédente n\'est pas un "%s".',
                $card->getName(),
                ((int) $card->getValue()) - 1
            ));
        }
    }
}
