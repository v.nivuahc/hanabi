<?php

namespace App\Domain\Collection;


use App\Domain\Exception\EmptyListOfCardsException;
use App\Domain\Model\Card\Card;

/**
 * Implémentation d'une liste de cartes.
 */
class ListOfCards implements ListOfCardsInterface
{
    /** @var Card[]|array */
    protected array $cards = [];

    /**
     * @inheritDoc
     */
    public function count(): int
    {
        return \count($this->cards);
    }

    /**
     * @inheritDoc
     */
    public function addCard(Card $card): void
    {
        $this->cards[] = $card;
    }

    /**
     * @inheritDoc
     */
    public function removeCard(Card $card): void
    {
        $cardIndex = array_search($card, $this->cards, true);
        if ($cardIndex !== false) {
            array_splice($this->cards, $cardIndex, 1);
        }
    }

    /**
     * @inheritDoc
     */
    public function removeFirst(): Card
    {
        if (\count($this->cards) === 0) {
            throw new EmptyListOfCardsException('La liste de cartes est vide, impossible d\'en retirer une.');
        }

        return array_shift($this->cards);
    }

    /**
     * @inheritDoc
     */
    public function getCards(): array
    {
        return $this->cards;
    }

    /**
     * @inheritDoc
     */
    public function getCard(int $id): ?Card
    {
        foreach ($this->cards as $card) {
            if ($card->getId() === $id) {
                return $card;
            }
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function contains(Card $card): bool
    {
        if ($card->getId() === null) {
            return false;
        }

        return $this->getCard($card->getId()) !== null;
    }
}
