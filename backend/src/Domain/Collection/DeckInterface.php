<?php

namespace App\Domain\Collection;

use App\Domain\Model\Card\Card;

interface DeckInterface extends ListOfCardsInterface
{
    /**
     * Mélange la liste des cartes.
     */
    public function shuffle(): DeckInterface;

    /**
     * Pioche la première carte de la pioche, si celle-ci n'est pas vide.
     *
     * @return Card|null La carte piochée, ou null si la pioche était vide
     */
    public function draw(): ?Card;
}
