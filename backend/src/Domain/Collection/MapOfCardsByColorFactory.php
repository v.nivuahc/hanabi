<?php

namespace App\Domain\Collection;

class MapOfCardsByColorFactory
{
    /**
     * Initie une nouvelle map de listes de cartes de valeurs successives.
     *
     * @return MapOfCardsByColor
     */
    public static function initMapOfSuccessiveCardLists(): MapOfCardsByColor
    {
        return new MapOfCardsByColor(ListOfSuccessiveCards::class);
    }

    /**
     * Initie une nouvelle map de listes de cartes triées par valeur.
     *
     * @return MapOfCardsByColor
     */
    public static function initMapOfSortedCardLists(): MapOfCardsByColor
    {
        return new MapOfCardsByColor(ListOfSortedCards::class);
    }
}
