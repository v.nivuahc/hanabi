<?php

namespace App\Domain\Collection;

use App\Domain\Model\Card\Card;
use App\Domain\Exception\EmptyListOfCardsException;
use App\Domain\Exception\InvalidCardAddException;

interface ListOfCardsInterface
{
    /**
     * Retourne le nombre de cartes dans la liste.
     *
     * @return int
     */
    public function count(): int;

    /**
     * Ajoute la carte à la fin de la liste.
     *
     * @param Card $card
     *
     * @throws InvalidCardAddException
     */
    public function addCard(Card $card): void;

    /**
     * Retire la carte de la liste (si elle s'y trouve).
     *
     * @param Card $card
     */
    public function removeCard(Card $card): void;

    /**
     * Retire la première carte de la liste.
     *
     * @throws EmptyListOfCardsException
     */
    public function removeFirst(): Card;

    /**
     * Retourne la liste des cartes de la liste.
     *
     * @return Card[]|array
     */
    public function getCards(): array;

    /**
     * Retourne la carte dont l'id est en argument, si elle est dans la liste.
     *
     * @param int $id
     *
     * @return Card|null
     */
    public function getCard(int $id): ?Card;

    /**
     * Retourne si la liste contient la carte en argument.
     *
     * @param Card $card
     *
     * @return bool
     */
    public function contains(Card $card): bool;
}
