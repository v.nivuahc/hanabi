<?php

namespace App\Domain\Collection;

use App\Domain\Model\Player;

class ListOfPlayers
{
    private const NB_OF_PLAYERS_MIN = 2;
    private const NB_OF_PLAYERS_MAX = 5;

    /** @var Player[]|array */
    private array $players;

    private Player $active;

    /**
     * @param Player[]|array $players
     */
    public function __construct(array $players)
    {
        $nbOfPlayers = \count($players);
        if ($nbOfPlayers < static::NB_OF_PLAYERS_MIN || $nbOfPlayers > static::NB_OF_PLAYERS_MAX) {
            throw new \InvalidArgumentException(sprintf(
                'Le nombre de joueurs doit être compris entre %s et %s',
                static::NB_OF_PLAYERS_MIN,
                static::NB_OF_PLAYERS_MAX
            ));
        }
        $this->players = $players;
        $this->chooseRandomActivePlayer();
    }

    /**
     * Retourne le nombre de joueurs de la liste.
     *
     * @return int
     */
    public function count(): int
    {
        return \count($this->players);
    }

    /**
     * Ajoute le joueur à la liste.
     *
     * @param Player $player
     */
    public function addPlayer(Player $player): void
    {
        $this->players[] = $player;
    }

    /**
     * Retourne le joueur suivant et en fait le joueur actif.
     */
    public function next(): Player
    {
        $activePlayerIndex = array_search($this->active, $this->players, true);
        $this->active = $this->players[($activePlayerIndex + 1) % ($this->count())];

        return $this->active;
    }

    /**
     * Retourne le joueur dont l'id est en argument.
     *
     * @param int $id
     *
     * @return Player|null
     */
    public function getPlayer(int $id): ?Player
    {
        foreach ($this->players as $playerItem) {
            if ($playerItem->getId() === $id) {
                return $playerItem;
            }
        }

        return null;
    }

    /**
     * Retourne l'index du joueur dont l'id est en argument.
     *
     * @param int|null $id
     *
     * @return int L'index ou -1 si aucun joueur avec cet id n'est présent
     */
    public function getPlayerIndex(?int $id): int
    {
        if ($id === null) {
            return -1;
        }

        $player = $this->getPlayer($id);
        if ($player === null) {
            return -1;
        }

        return array_search($player, $this->players, true);
    }

    /**
     * @return Player[]|array
     */
    public function getPlayers(): array
    {
        return $this->players;
    }

    public function getActive(): Player
    {
        return $this->active;
    }

    public function setActive(Player $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Retourne un joueur de la liste au hasard, et en fait le joueur actif.
     *
     * @return Player
     */
    private function chooseRandomActivePlayer(): Player
    {
        $this->active = $this->players[random_int(0, $this->count() - 1)];

        return $this->active;
    }
}
