<?php

namespace App\Domain\Collection;

use App\Domain\Exception\EmptyListOfCardsException;
use App\Domain\Model\Card\Card;
use App\Domain\Model\Card\CardType;
use App\Domain\Model\Card\Color;
use App\Domain\Model\Card\Value;

/**
 * Implémentation d'une pioche.
 */
class Deck extends ListOfCards implements DeckInterface
{
    /**
     * @param Card[]|array $cards
     */
    public function __construct(array $cards)
    {
        foreach ($cards as $card) {
            $this->addCard($card);
        }
    }

    /**
     * Initialise un nouveau deck avec la répartition de cartes spécifiée.
     *
     * @param int[][]|array $cardRepartitionByColor
     *
     * @return Deck
     */
    public static function init(array $cardRepartitionByColor): self
    {
        $instance = new self([]);

        foreach ($cardRepartitionByColor as $colorName => $values) {
            $color = new Color($colorName);

            foreach ($values as $value) {
                $instance->addCard(new Card(
                    new CardType(new Value((string) $value), $color)
                ));
            }
        }

        return $instance;
    }

    /**
     * Mélange la pioche.
     */
    public function shuffle(): self
    {
        shuffle($this->cards);
        $this->cards = array_values(
            array_map(
                fn(Card $card, int $index) => $card->setIndex($index),
                $this->cards,
                array_keys($this->cards)
            )
        );

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function draw(): ?Card
    {
        $drawnCard = null;

        try {
            $drawnCard = $this->removeFirst();
        } catch (EmptyListOfCardsException $e) {

        }

        return $drawnCard;
    }

    /**
     * Retourne le nombre de cartes restantes dans la pioche.
     *
     * @return int
     */
    public function getNbOfRemainingCards(): int
    {
        return $this->count();
    }
}
