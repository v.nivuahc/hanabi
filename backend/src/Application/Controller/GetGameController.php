<?php

namespace App\Application\Controller;

use App\Application\Service\GameRetriever;
use App\Application\Service\ToFrontEndConverter;
use App\Domain\Exception\HanabiExceptionInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GetGameController
{
    private GameRetriever $gameRetriever;
    private ToFrontEndConverter $toFrontendConverter;

    public function __construct(GameRetriever $gameRetriever, ToFrontEndConverter $toFrontendConverter)
    {
        $this->gameRetriever = $gameRetriever;
        $this->toFrontendConverter = $toFrontendConverter;
    }

    /**
     * @Route("/api/game/{id}", name="game_get", requirements={"id"="\d+"})
     *
     * @param int     $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function get(int $id, Request $request): JsonResponse
    {
        $game = $this->gameRetriever->retrieve($id);
        if ($game === null) {
            return new JsonResponse('Partie non trouvée', Response::HTTP_NOT_FOUND);
        }

        $playerId = (int) $request->query->get('player_id', 0);
        $currentPlayer = $game->getPlayers()->getPlayer($playerId);
        if ($currentPlayer === null) {
            return new JsonResponse('Joueur inconnu', Response::HTTP_NOT_FOUND);
        }

        try {
            $gameForFrontend = $this->toFrontendConverter->convertGame($game, $currentPlayer);
        } catch (HanabiExceptionInterface $e) {
            return new JsonResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse($gameForFrontend);
    }
}
