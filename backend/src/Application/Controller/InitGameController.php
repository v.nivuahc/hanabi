<?php

namespace App\Application\Controller;

use App\Application\Service\GameInitiator;
use App\Application\Service\ToFrontEndConverter;
use App\Domain\Exception\HanabiExceptionInterface;
use App\Domain\Model\Game\GameFactory;
use App\Domain\Model\Game\Config\NormalConfig;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InitGameController
{
    private GameInitiator $gameInitiator;
    private ToFrontEndConverter $toFrontendConverter;

    public function __construct(GameInitiator $gameInitiator, ToFrontEndConverter $toFrontendConverter)
    {
        $this->gameInitiator = $gameInitiator;
        $this->toFrontendConverter = $toFrontendConverter;
    }

    /**
     * @Route("/api/game/init", name="game_init")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function initGame(Request $request): JsonResponse
    {
        $mode = $request->query->get('mode', NormalConfig::MODE);
        $nbPlayers = (int) $request->query->get('nb_players', GameFactory::DEFAULT_NB_OF_PLAYERS);

        try {
            $game = $this->gameInitiator->init($mode, $nbPlayers);
            $gameForFrontend = $this->toFrontendConverter->convertGame($game);
        } catch (HanabiExceptionInterface $e) {
            return new JsonResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse($gameForFrontend);
    }
}
