<?php

namespace App\Application\Controller;

use App\Application\Service\GameRetriever;
use App\Application\Service\ToFrontEndConverter;
use App\Domain\Action\ActionExecutor;
use App\Domain\Exception\HanabiExceptionInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExecuteActionController
{
    private GameRetriever $gameRetriever;
    private ActionExecutor $actionDoer;
    private ToFrontEndConverter $toFrontendConverter;

    public function __construct(
        GameRetriever $gameRetriever,
        ActionExecutor $actionDoer,
        ToFrontEndConverter $toFrontendConverter
    ) {
        $this->gameRetriever = $gameRetriever;
        $this->actionDoer = $actionDoer;
        $this->toFrontendConverter = $toFrontendConverter;
    }

    /**
     * @Route("/api/game/{id}/action/{type}/{actorId}", name="action_do", requirements={"id"="\d+", "actorId"="\d+"})
     *
     * @param int     $id
     * @param string  $type
     * @param int     $actorId
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function execute(int $id, string $type, int $actorId, Request $request): JsonResponse
    {
        $game = $this->gameRetriever->retrieve($id);
        if ($game === null) {
            return new JsonResponse('Partie non trouvée', Response::HTTP_NOT_FOUND);
        }

        $activePlayer = $game->getPlayers()->getActive();
        if ($activePlayer->getId() !== $actorId) {
            return new JsonResponse('Seul le joueur actif peut effectuer une action', Response::HTTP_FORBIDDEN);
        }

        try {
            $body = (string) $request->getContent();
            $args = json_decode($body, true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $e) {
            return new JsonResponse('Flux JSON invalide', Response::HTTP_BAD_REQUEST);
        }

        try {
            $this->actionDoer->execute($type, $game, $activePlayer, $args);
            $gameForFrontend = $this->toFrontendConverter->convertGame($game, $activePlayer);
        } catch (HanabiExceptionInterface $e) {
            return new JsonResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse($gameForFrontend);
    }
}
