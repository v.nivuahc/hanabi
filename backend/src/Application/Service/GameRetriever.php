<?php

namespace App\Application\Service;

use App\Domain\Model\Game\Game;
use App\Infrastructure\Persistence\Persister;
use App\Infrastructure\Persistence\ToDomainConverter;

class GameRetriever
{
    private Persister $persister;
    private ToDomainConverter $toDomainConverter;

    public function __construct(
        Persister $persister,
        ToDomainConverter $toDomainConverter
    ) {
        $this->persister = $persister;
        $this->toDomainConverter = $toDomainConverter;
    }

    /**
     * Récupère la partie.
     *
     * @param int $gameId
     *
     * @return Game|null
     */
    public function retrieve(int $gameId): ?Game
    {
        $gameEntity = $this->persister->retrieve($gameId);

        if ($gameEntity === null) {
            return null;
        }

        return $this->toDomainConverter->convertGame($gameEntity);
    }
}
