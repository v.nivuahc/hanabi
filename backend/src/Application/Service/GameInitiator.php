<?php

namespace App\Application\Service;

use App\Domain\Model\Game\Game;
use App\Domain\Model\Game\GameFactory;
use App\Infrastructure\Persistence\Persister;
use App\Infrastructure\Persistence\ToDomainConverter;

class GameInitiator
{
    private GameFactory $gameFactory;
    private Persister $persister;
    private ToDomainConverter $toDomainConverter;

    public function __construct(
        GameFactory $gameFactory,
        Persister $persister,
        ToDomainConverter $toDomainConverter
    ) {
        $this->gameFactory = $gameFactory;
        $this->persister = $persister;
        $this->toDomainConverter = $toDomainConverter;
    }

    /**
     * Initialise une partie pour le mode et le nombre de joueurs spécifiés.
     *
     * La partie est persistée avant d'être retournée.
     *
     * @param string $mode
     * @param int    $nbPlayers
     *
     * @return Game
     */
    public function init(string $mode, int $nbPlayers): Game
    {
        $game = $this->gameFactory->initGame($mode, $nbPlayers);

        $gameEntity = $this->persister->persist($game);
        $game = null;

        return $this->toDomainConverter->convertGame($gameEntity);
    }
}
