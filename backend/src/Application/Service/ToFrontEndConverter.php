<?php

namespace App\Application\Service;

use App\Domain\Collection\ListOfCardsInterface;
use App\Domain\Collection\ListOfPlayers;
use App\Domain\Collection\MapOfCardsByColor;
use App\Domain\Model\Card\Card;
use App\Domain\Model\Game\Game;
use App\Domain\Model\History\History;
use App\Domain\Model\History\Message;
use App\Domain\Model\Player;

class ToFrontEndConverter
{
    /**
     * Convertit la partie en une structure de tableaux associatifs exploitable côté FrontEnd.
     *
     * @param Game        $game
     * @param Player|null $currentPlayer Joueur pour qui générer la structure
     *
     * @return mixed[]|array
     */
    public function convertGame(Game $game, ?Player $currentPlayer = null): array
    {
        $gameConfig = $game->getConfig();
        $playerRotation = $this->rotatePlayers($game->getPlayers(), $currentPlayer);

        return [
            'id' => $game->getId(),
            'mode' => $gameConfig->getMode(),
            'colors' => $gameConfig->getColorNames(),
            'nbCardsInDeck' => $game->getDeck()->count(),
            'nbClueTokens' => $game->getNbOfRemainingClueToken(),
            'nbClueTokenMax' => $gameConfig->getNbOfClueTokens(),
            'nbBombs' => $gameConfig->getNbOfAcceptableBombs() - $game->getNbOfRemainingBombsBeforeLose(),
            'nbBombMax' => $gameConfig->getNbOfAcceptableBombs(),
            'showMulti' => $gameConfig->isShowMultiClue(),
            'playedCards' => $this->getPlayedCards($game->getPlayedCards()),
            'discardedCards' => $this->getDiscardedCards($game->getDiscard()),
            'players' => $this->getPlayers($playerRotation['list']),
            'activePlayerIndex' => $playerRotation['active'],
            'history' => $this->getHistory($game->getHistory(), $currentPlayer),
            'status' => $game->getStatus(),
        ];
    }

    /**
     * Retourne la valeur de la plus grande carte jouée dans chaque couleur.
     *
     * @param MapOfCardsByColor $playedCardMap
     *
     * @return array<string|null>
     */
    private function getPlayedCards(MapOfCardsByColor $playedCardMap): array
    {
        $playedCards = [];

        foreach ($playedCardMap->getCardsByColor() as $color => $listOfCards) {
            $cards = $listOfCards->getCards();
            /** @var Card|false $lastCardOfColor */
            $lastCardOfColor = end($cards);
            $playedCards[$color] = $lastCardOfColor === false ? null : $lastCardOfColor->getValue();
        }

        return $playedCards;
    }

    /**
     * Retourne la liste des cartes défaussées par couleur.
     *
     * @param MapOfCardsByColor $discardedCardMap
     *
     * @return array<string, array<string>>
     */
    private function getDiscardedCards(MapOfCardsByColor $discardedCardMap): array
    {
        $discardedCards = [];

        /** @var string $color */
        foreach ($discardedCardMap->getCardsByColor() as $color => $cards) {
            if ($cards->count() === 0) {
                $discardedCards[$color] = [];
                continue;
            }

            foreach ($cards->getCards() as $card) {
                $discardedCards[$color][] = $card->getValue();
            }
        }

        return $discardedCards;
    }

    /**
     * Retourne la liste des joueurs en une structure exploitable côté FrontEnd.
     *
     * @param Player[]|array $players
     *
     * @return mixed[]|array
     */
    private function getPlayers(array $players): array
    {
        $list = [];
        foreach ($players as $index => $player) {
            $playerForFrontend = [
                'id' => $player->getId(),
                'name' => $player->getName(),
                'hand' => $this->getHand($player->getHand(), $index === 0),
            ];

            $list[] = $playerForFrontend;
        }

        return $list;
    }

    /**
     * Fait tourner les joueurs pour placer en premier, celui pour qui on génère les données.
     *
     * @param ListOfPlayers $players
     * @param Player|null   $currentPlayer Joueur pour qui on génère les données
     *
     * @return mixed[]|array la liste modifiée et le nouvel index du joueur actif
     */
    private function rotatePlayers(ListOfPlayers $players, ?Player $currentPlayer): array
    {
        $currentPlayerIndex = 0;
        if ($currentPlayer !== null) {
            $currentPlayerIndex = $players->getPlayerIndex($currentPlayer->getId());
            if ($currentPlayerIndex === -1) {
                throw new \InvalidArgumentException(sprintf(
                    'Le joueur "%s" n\'est pas dans la partie.',
                    $currentPlayer->getName()
                ));
            }
        }

        $activePlayerIndex = $players->getPlayerIndex($players->getActive()->getId());
        if ($activePlayerIndex === -1) {
            throw new \InvalidArgumentException('Aucun joueur actif dans la partie.');
        }

        $calcNewIndex = fn($oldIndex) => ($oldIndex + $currentPlayerIndex) % ($players->count());

        $rotated = [];
        foreach ($players->getPlayers() as $index => $player) {
            $rotated[$calcNewIndex($index)] = $player;
        }
        ksort($rotated);

        return [
            'list' => $rotated,
            'active' => $calcNewIndex($activePlayerIndex),
        ];
    }

    /**
     * Retourne la liste des messages de l'historique.
     *
     * @param History     $history
     *
     * @param Player|null $currentPlayer
     *
     * @return string[]|array
     */
    private function getHistory(History $history, ?Player $currentPlayer): array
    {
        return array_values(
            array_map(
                fn(Message $message) => sprintf(
                    '{%s}%s{/%s}',
                    $message->getType(),
                    $message->getContent($currentPlayer),
                    $message->getType()
                ),
                array_filter(
                    $history->getMessages(),
                    fn(Message $message) => !empty($message->getContent($currentPlayer)),
                )
            )
        );
    }

    /**
     * Retourne la liste des cartes de la main.
     *
     * @param ListOfCardsInterface $hand
     * @param bool                 $hideCardInfo Si le détail de la carte doit être masqué et remplacé par son id
     *
     * @return mixed[]|array
     */
    private function getHand(ListOfCardsInterface $hand, bool $hideCardInfo): array
    {
        $cards = [];
        foreach ($hand->getCards() as $card) {
            $cards[] = $this->getCard($card, $hideCardInfo);
        }

        return $cards;
    }

    /**
     * Retourne la carte en une structure exploitable côté FrontEnd.
     *
     * @param Card $card
     * @param bool $hideCardInfo Si le détail de la carte doit être masqué et remplacé par son id
     *
     * @return mixed[]|array
     */
    private function getCard(Card $card, bool $hideCardInfo): array
    {
        $cardForFrontend = [
            'color' => $hideCardInfo ? null : $card->getColor()->getName(),
            'value' => $hideCardInfo ? null : $card->getValue(),
            'clues' => [
                'color' => $card->getColorClue() !== null ? $card->getColorClue()->getColor()->getName() : null,
                'value' => $card->getValueClue() !== null ? $card->getValueClue()->getValue() : null,
            ],
        ];

        if ($hideCardInfo) {
            $cardForFrontend['id'] = $card->getId();
        }

        return $cardForFrontend;
    }
}
