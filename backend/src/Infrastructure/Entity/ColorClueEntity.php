<?php

namespace App\Infrastructure\Entity;

use App\Domain\Model\Clue\ColorClue;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="color_clue")
 */
class ColorClueEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $color;

    /**
     * Instancie un nouvel objet, hydraté à partir de son pendant provenant du domaine.
     *
     * @param ColorClue            $clue
     *
     * @param ColorClueEntity|null $clueEntity
     *
     * @return ColorClueEntity
     */
    public static function fromDomain(ColorClue $clue, ?self $clueEntity = null): self
    {
        $instance = $clueEntity ?? new self();

        return $instance->hydrateFromDomain($clue);
    }

    /**
     * Hydrate l'objet courant à partir de son pendant provenant du domaine.
     *
     * @param ColorClue $colorClue
     *
     * @return ColorClueEntity
     */
    public function hydrateFromDomain(ColorClue $colorClue): self
    {
        return $this
            ->setColor($colorClue->getColor()->getName());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }
}
