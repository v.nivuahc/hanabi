<?php

namespace App\Infrastructure\Entity;

use App\Domain\Model\Clue\ValueClue;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="value_clue")
 */
class ValueClueEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $value;

    /**
     * Instancie un nouvel objet, hydraté à partir de son pendant provenant du domaine.
     *
     * @param ValueClue            $clue
     *
     * @param ValueClueEntity|null $clueEntity
     *
     * @return ValueClueEntity
     */
    public static function fromDomain(ValueClue $clue, ?self $clueEntity = null): self
    {
        $instance = $clueEntity ?? new self();

        return $instance->hydrateFromDomain($clue);
    }

    /**
     * Hydrate l'objet courant à partir de son pendant provenant du domaine.
     *
     * @param ValueClue $valueClue
     *
     * @return ValueClueEntity
     */
    public function hydrateFromDomain(ValueClue $valueClue): self
    {
        return $this
            ->setValue($valueClue->getValue());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
