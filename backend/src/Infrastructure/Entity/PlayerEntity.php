<?php

namespace App\Infrastructure\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="player")
 */
class PlayerEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name;

    /**
     * @var Collection<int, CardEntity>|CardEntity[]
     *
     * @ORM\ManyToMany(targetEntity="CardEntity", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="players_cards",
     *      joinColumns={@ORM\JoinColumn(name="player_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="card_id", referencedColumnName="id", unique=true)}
     * )
     * @ORM\OrderBy({"index"="ASC"})
     */
    private Collection $hand;

    /**
     * @var GameEntity
     *
     * @ORM\ManyToOne(targetEntity="GameEntity", inversedBy="players")
     */
    private GameEntity $game;

    public function __construct()
    {
        $this->hand = new ArrayCollection();
    }

    /**
     * Vide la main du joueur.
     *
     * @return $this
     */
    public function resetHand(): self
    {
        $this->hand = new ArrayCollection();

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, CardEntity>|CardEntity[]
     */
    public function getHand(): Collection
    {
        return $this->hand;
    }

    public function addToHand(CardEntity $card): self
    {
        if (!$this->hand->contains($card)) {
            $this->hand[] = $card;
        }

        return $this;
    }

    public function removeFromHand(?CardEntity $card): self
    {
        if ($card !== null && $this->hand->contains($card)) {
            $this->hand->removeElement($card);
        }

        return $this;
    }

    public function getGame(): GameEntity
    {
        return $this->game;
    }

    public function setGame(GameEntity $game): self
    {
        $this->game = $game;

        return $this;
    }
}
