<?php

namespace App\Infrastructure\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="card")
 */
class CardEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private int $index;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $color;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $value;

    /**
     * @ORM\OneToOne(targetEntity="ColorClueEntity", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private ?ColorClueEntity $colorClue = null;

    /**
     * @ORM\OneToOne(targetEntity="ValueClueEntity", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private ?ValueClueEntity $valueClue = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getIndex(): int
    {
        return $this->index;
    }

    public function setIndex(int $index): self
    {
        $this->index = $index;

        return $this;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getColorClue(): ?ColorClueEntity
    {
        return $this->colorClue;
    }

    public function setColorClue(?ColorClueEntity $colorClue): self
    {
        $this->colorClue = $colorClue;

        return $this;
    }

    public function getValueClue(): ?ValueClueEntity
    {
        return $this->valueClue;
    }

    public function setValueClue(?ValueClueEntity $valueClue): self
    {
        $this->valueClue = $valueClue;

        return $this;
    }
}
