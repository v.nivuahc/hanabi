<?php

namespace App\Infrastructure\Entity;

use App\Domain\Model\Game\Game;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="game")
 */
class GameEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $mode;

    /**
     * @var Collection<int, CardEntity>|CardEntity[]
     *
     * @ORM\ManyToMany(targetEntity="CardEntity", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="game_decks_cards",
     *      joinColumns={@ORM\JoinColumn(name="game_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="card_id", referencedColumnName="id", unique=true)}
     * )
     * @ORM\OrderBy({"index"="ASC"})
     */
    private Collection $deck;

    /**
     * @var Collection<int, CardEntity>|CardEntity[]
     *
     * @ORM\ManyToMany(targetEntity="CardEntity", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="game_discards_cards",
     *      joinColumns={@ORM\JoinColumn(name="game_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="card_id", referencedColumnName="id", unique=true)}
     * )
     * @ORM\OrderBy({"value"="ASC"})
     */
    private Collection $discard;

    /**
     * @var Collection<int, CardEntity>|CardEntity[]
     * @ORM\ManyToMany(targetEntity="CardEntity", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="game_played_cards_cards",
     *      joinColumns={@ORM\JoinColumn(name="game_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="card_id", referencedColumnName="id", unique=true)}
     * )
     * @ORM\OrderBy({"value"="ASC"})
     */
    private Collection $playedCards;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private ?int $nbOfRemainingClueToken;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private ?int $nbOfRemainingBombsBeforeLose;

    /**
     * @var Collection<int, PlayerEntity>|PlayerEntity[]
     *
     * @ORM\OneToMany(targetEntity="PlayerEntity", mappedBy="game", cascade={"persist", "remove"})
     */
    private Collection $players;

    /**
     * @ORM\OneToOne(targetEntity="PlayerEntity")
     * @ORM\JoinColumn(name="active_player_id", referencedColumnName="id", nullable=true)
     */
    private ?PlayerEntity $activePlayer;

    /**
     * @var Collection<int, MessageEntity>|MessageEntity[]
     *
     * @ORM\OneToMany(targetEntity="MessageEntity", mappedBy="game", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"id"="ASC"})
     */
    private Collection $history;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $status;

    public function __construct()
    {
        $this->resetCollections();
        $this->history = new ArrayCollection();
        $this->status = Game::STATUS_RUNNING;
    }

    /**
     * Vide toutes les collections.
     *
     * @return $this
     */
    public function resetCollections(): self
    {
        $this->players = new ArrayCollection();
        $this->deck = new ArrayCollection();
        $this->discard = new ArrayCollection();
        $this->playedCards = new ArrayCollection();

        return $this;
    }

    /**
     * Retourne le joueur dont l'id est en argument.
     *
     * @param int $playerId
     *
     * @return PlayerEntity|null
     */
    public function getPlayerById(int $playerId): ?PlayerEntity
    {
        return $this->getPlayers()
            ->filter(fn(PlayerEntity $player) => $player->getId() === $playerId)
            ->first() ?: null;
    }

    /**
     * Retourne la carte dont l'id est en argument.
     *
     * La carte est recherchée dans la main des joueurs, le deck, la défausse et parmi les cartes jouées.
     *
     * @param int $cardId
     *
     * @return CardEntity|null
     */
    public function getCardById(int $cardId): ?CardEntity
    {
        foreach ($this->getPlayers() as $player) {
            $cardFromPlayerHand = $player->getHand()
                ->filter(fn(CardEntity $card) => $card->getId() === $cardId)
                ->first();

            if (!empty($cardFromPlayerHand)) {
                return $cardFromPlayerHand;
            }
        }

        $cardFromDeck = $this->getDeck()
            ->filter(fn(CardEntity $card) => $card->getId() === $cardId)
            ->first();

        if (!empty($cardFromDeck)) {
            return $cardFromDeck;
        }

        $cardFromDiscard = $this->getDiscard()
            ->filter(fn(CardEntity $card) => $card->getId() === $cardId)
            ->first();

        if (!empty($cardFromDiscard)) {
            return $cardFromDiscard;
        }

        $cardPlayed = $this->getPlayedCards()
            ->filter(fn(CardEntity $card) => $card->getId() === $cardId)
            ->first();

        if (!empty($cardPlayed)) {
            return $cardPlayed;
        }

        return null;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return GameEntity
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getMode(): string
    {
        return $this->mode;
    }

    public function setMode(string $mode): self
    {
        $this->mode = $mode;

        return $this;
    }

    public function getNbOfRemainingClueToken(): ?int
    {
        return $this->nbOfRemainingClueToken;
    }

    public function setNbOfRemainingClueToken(int $nbOfRemainingClueToken): self
    {
        $this->nbOfRemainingClueToken = $nbOfRemainingClueToken;

        return $this;
    }

    public function getNbOfRemainingBombsBeforeLose(): ?int
    {
        return $this->nbOfRemainingBombsBeforeLose;
    }

    public function setNbOfRemainingBombsBeforeLose(int $nbOfRemainingBombsBeforeLose): self
    {
        $this->nbOfRemainingBombsBeforeLose = $nbOfRemainingBombsBeforeLose;

        return $this;
    }

    /**
     * @return Collection<int, CardEntity>|CardEntity[]
     */
    public function getDeck(): Collection
    {
        return $this->deck;
    }

    public function addToDeck(CardEntity $card): self
    {
        if (!$this->deck->contains($card)) {
            $this->deck[] = $card;
        }

        return $this;
    }

    public function removeFromDeck(?CardEntity $card): self
    {
        if ($card !== null && $this->deck->contains($card)) {
            $this->deck->removeElement($card);
        }

        return $this;
    }

    /**
     * @return Collection<int, CardEntity>|CardEntity[]
     */
    public function getDiscard(): Collection
    {
        return $this->discard;
    }

    public function addToDiscard(CardEntity $card): self
    {
        if (!$this->discard->contains($card)) {
            $this->discard[] = $card;
        }

        return $this;
    }

    public function removeFromDiscard(?CardEntity $card): self
    {
        if ($card !== null && $this->discard->contains($card)) {
            $this->discard->removeElement($card);
        }

        return $this;
    }

    /**
     * @return Collection<int, CardEntity>|CardEntity[]
     */
    public function getPlayedCards(): Collection
    {
        return $this->playedCards;
    }

    public function addPlayedCard(CardEntity $card): self
    {
        if (!$this->playedCards->contains($card)) {
            $this->playedCards[] = $card;
        }

        return $this;
    }

    public function removePlayedCard(?CardEntity $playedCard): self
    {
        if ($playedCard !== null && $this->playedCards->contains($playedCard)) {
            $this->playedCards->removeElement($playedCard);
        }

        return $this;
    }

    /**
     * @return Collection<int, PlayerEntity>|PlayerEntity[]
     */
    public function getPlayers(): Collection
    {
        return $this->players;
    }

    public function addPlayer(PlayerEntity $player): self
    {
        if (!$this->players->contains($player)) {
            $player->setGame($this);
            $this->players[] = $player;
        }

        return $this;
    }

    public function removePlayer(?PlayerEntity $player): self
    {
        if ($player !== null && $this->players->contains($player)) {
            $this->players->removeElement($player);
        }

        return $this;
    }

    public function getActivePlayer(): ?PlayerEntity
    {
        return $this->activePlayer;
    }

    public function setActivePlayer(?PlayerEntity $activePlayer): self
    {
        $this->activePlayer = $activePlayer;

        return $this;
    }

    /**
     * @return Collection<int, MessageEntity>|MessageEntity[]
     */
    public function getHistory(): Collection
    {
        return $this->history;
    }

    public function addMessageToHistory(MessageEntity $message): self
    {
        if (!$this->history->contains($message)) {
            $message->setGame($this);
            $this->history[] = $message;
        }

        return $this;
    }

    public function removeMessageFromHistory(?MessageEntity $message): self
    {
        if ($message !== null && $this->history->contains($message)) {
            $this->history->removeElement($message);
        }

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
