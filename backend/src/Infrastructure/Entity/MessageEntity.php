<?php

namespace App\Infrastructure\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="message")
 */
class MessageEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $type;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private \DateTime $createdAt;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $class;

    /**
     * @var GameEntity|null
     *
     * @ORM\ManyToOne(targetEntity="GameEntity", inversedBy="history")
     */
    private ?GameEntity $game = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $content;

    /**
     * @var PlayerEntity|null
     *
     * @ORM\ManyToOne(targetEntity="PlayerEntity")
     * @ORM\JoinColumn(name="actor_player_id", referencedColumnName="id", nullable=true)
     */
    private ?PlayerEntity $actor = null;

    /**
     * @var CardEntity|null
     *
     * @ORM\ManyToOne(targetEntity="CardEntity")
     * @ORM\JoinColumn(name="card_id", referencedColumnName="id", nullable=true)
     */
    private ?CardEntity $card = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $clue = null;

    /**
     * @var PlayerEntity|null
     *
     * @ORM\ManyToOne(targetEntity="PlayerEntity")
     * @ORM\JoinColumn(name="target_player_id", referencedColumnName="id", nullable=true)
     */
    private ?PlayerEntity $target = null;

    /**
     * Définie la partie auquel se rattache le message, ainsi que les joueurs concernés.
     *
     * @param GameEntity $game
     *
     * @return $this
     */
    public function setGame(GameEntity $game): self
    {
        $this->game = $game;

        // Réutilisation des joueurs de la partie, pour éviter d'en persister de nouveaux
        if ($this->actor !== null && $this->actor->getId() !== null) {
            $actor = $this->game->getPlayerById($this->actor->getId());
            if ($actor !== null) {
                $this->setActor($actor);
            }
        }

        if ($this->target !== null && $this->target->getId() !== null) {
            $targetPlayer = $this->game->getPlayerById($this->target->getId());
            if ($targetPlayer !== null) {
                $this->setTarget($targetPlayer);
            }
        }

        if ($this->card !== null && $this->card->getId() !== null) {
            $card = $this->game->getCardById($this->card->getId());
            if ($card !== null) {
                $this->setCard($card);
            }
        }

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getClass(): string
    {
        return $this->class;
    }

    public function setClass(string $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function getGame(): ?GameEntity
    {
        return $this->game;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getActor(): ?PlayerEntity
    {
        return $this->actor;
    }

    public function setActor(?PlayerEntity $actor): self
    {
        $this->actor = $actor;

        return $this;
    }

    public function getCard(): ?CardEntity
    {
        return $this->card;
    }

    public function setCard(?CardEntity $card): self
    {
        $this->card = $card;

        return $this;
    }

    public function getClue(): ?string
    {
        return $this->clue;
    }

    public function setClue(?string $clue): self
    {
        $this->clue = $clue;

        return $this;
    }

    public function getTarget(): ?PlayerEntity
    {
        return $this->target;
    }

    public function setTarget(?PlayerEntity $target): self
    {
        $this->target = $target;

        return $this;
    }
}
