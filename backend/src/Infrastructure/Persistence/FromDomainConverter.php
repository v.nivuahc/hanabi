<?php

namespace App\Infrastructure\Persistence;

use App\Domain\Action\Message\ActionMessage;
use App\Domain\Action\Message\CardActionMessage;
use App\Domain\Action\Message\ClueMessage;
use App\Domain\Collection\DeckInterface;
use App\Domain\Model\Card\Card;
use App\Domain\Model\Game\Game;
use App\Domain\Model\History\Message;
use App\Domain\Model\Player;
use App\Infrastructure\Entity\CardEntity;
use App\Infrastructure\Entity\ColorClueEntity;
use App\Infrastructure\Entity\GameEntity;
use App\Infrastructure\Entity\MessageEntity;
use App\Infrastructure\Entity\PlayerEntity;
use App\Infrastructure\Entity\ValueClueEntity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class FromDomainConverter
{
    private ObjectRepository $gameRepository;
    private ObjectRepository $cardRepository;
    private ObjectRepository $playerRepository;
    private ObjectRepository $messageRepository;

    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->gameRepository = $objectManager->getRepository(GameEntity::class);
        $this->cardRepository = $objectManager->getRepository(CardEntity::class);
        $this->playerRepository = $objectManager->getRepository(PlayerEntity::class);
        $this->messageRepository = $objectManager->getRepository(MessageEntity::class);
    }

    /**
     * Retourne le pendant de l'entité dans le domaine.
     *
     * @param Game $game
     *
     * @return GameEntity
     */
    public function convertGame(Game $game): GameEntity
    {
        $gameEntity = new GameEntity();
        if ($game->getId() !== null) {
            /** @var GameEntity $gameEntity */
            $gameEntity = $this->gameRepository->find($game->getId());
            $gameEntity->resetCollections();
        }

        $this->hydrateDeckFromDomain($game->getDeck(), $gameEntity);
        $this->hydrateDiscardFromDomain($game->getDiscard()->getAll(), $gameEntity);
        $this->hydratePlayedCardsFromDomain($game->getPlayedCards()->getAll(), $gameEntity);

        $activePlayer = null;
        foreach ($game->getPlayers()->getPlayers() as $player) {
            $playerEntity = $this->hydratePlayer($player);
            $gameEntity->addPlayer($playerEntity);
            if ($player->getName() === $game->getPlayers()->getActive()->getName()
                || ($player->getId() !== null && $player->getId() === $game->getPlayers()->getActive()->getId())
            ) {
                $activePlayer = $playerEntity;
            }
        }

        $this->hydrateHistoryFromDomain($game->getHistory()->getMessages(), $gameEntity);

        return $gameEntity
            ->setId($game->getId())
            ->setMode($game->getConfig()->getMode())
            ->setNbOfRemainingBombsBeforeLose($game->getNbOfRemainingBombsBeforeLose())
            ->setNbOfRemainingClueToken($game->getNbOfRemainingClueToken())
            ->setActivePlayer($activePlayer)
            ->setStatus(($game->getStatus()));
    }

    /**
     * Hydrate le deck de l'objet courant à partir de son pendant provenant du domaine.
     *
     * @param DeckInterface $deck
     * @param GameEntity    $gameEntity
     */
    private function hydrateDeckFromDomain(DeckInterface $deck, GameEntity $gameEntity): void
    {
        foreach ($deck->getCards() as $card) {
            $gameEntity->addToDeck($this->convertCard($card));
        }
    }

    /**
     * Hydrate la défausse de l'objet courant à partir de son pendant provenant du domaine.
     *
     * @param Card[]|array $discard
     * @param GameEntity   $gameEntity
     */
    private function hydrateDiscardFromDomain(array $discard, GameEntity $gameEntity): void
    {
        foreach ($discard as $card) {
            $gameEntity->addToDiscard($this->convertCard($card));
        }
    }

    /**
     * Hydrate les cartes jouées de l'objet courant à partir de leur pendant provenant du domaine.
     *
     * @param Card[]|array $playedCards
     * @param GameEntity   $gameEntity
     */
    private function hydratePlayedCardsFromDomain(array $playedCards, GameEntity $gameEntity): void
    {
        foreach ($playedCards as $card) {
            $gameEntity->addPlayedCard($this->convertCard($card));
        }
    }

    /**
     * Hydrate les messages de l'historique à partir de leurs pendants provenant du domaine.
     *
     * @param Message[]|array $messages
     * @param GameEntity      $gameEntity
     */
    private function hydrateHistoryFromDomain(array $messages, GameEntity $gameEntity): void
    {
        foreach ($messages as $message) {
            if ($message->getId() === null) {
                $gameEntity->addMessageToHistory($this->convertMessage($message));
            }
        }
    }

    /**
     * Hydrate le joueur à partir de son pendant provenant du domaine.
     *
     * @param Player $player
     *
     * @return PlayerEntity
     */
    private function hydratePlayer(Player $player): PlayerEntity
    {
        $playerEntity = new PlayerEntity();
        if ($player->getId() !== null) {
            /** @var PlayerEntity $playerEntity */
            $playerEntity = $this->playerRepository->find($player->getId());
            $playerEntity->resetHand();
        }

        foreach ($player->getHand()->getCards() as $card) {
            $playerEntity->addToHand($this->convertCard($card));
        }

        return $playerEntity
            ->setName($player->getName());
    }

    /**
     * Hydrate la carte à partir de son pendant provenant du domaine.
     *
     * @param Card $card
     *
     * @return CardEntity
     */
    private function convertCard(Card $card): CardEntity
    {
        $cardEntity = new CardEntity();
        if ($card->getId() !== null) {
            /** @var CardEntity|null $cardEntity */
            $cardEntity = $this->cardRepository->find($card->getId());
            if ($cardEntity === null) {
                throw new \InvalidArgumentException(sprintf(
                    'Aucune carte en base avec l\'id %s',
                    $card->getId()
                ));
            }
        }

        $colorClue = $card->getColorClue() === null
            ? null
            : ColorClueEntity::fromDomain($card->getColorClue(), $cardEntity->getColorClue());
        $valueClue = $card->getValueClue() === null
            ? null
            : ValueClueEntity::fromDomain($card->getValueClue(), $cardEntity->getValueClue());

        return $cardEntity
            ->setId($card->getId())
            ->setIndex($card->getIndex())
            ->setColorClue($colorClue)
            ->setValueClue($valueClue)
            ->setColor($card->getType()->getColor()->getName())
            ->setValue($card->getType()->getValue()->getValue());
    }

    /**
     * Hydrate le message à partir de son pendant provenant du domaine.
     *
     * @param Message $message
     *
     * @return MessageEntity
     */
    private function convertMessage(Message $message): MessageEntity
    {
        $messageEntity = new MessageEntity();
        if ($message->getId() !== null) {
            /** @var MessageEntity $messageEntity */
            $messageEntity = $this->messageRepository->find($message->getId());
        }

        if ($message instanceof ActionMessage) {
            $messageEntity->setActor(
                $this->hydratePlayer($message->getActor())
            );
        }
        if ($message instanceof CardActionMessage) {
            $messageEntity->setCard(
                $this->convertCard($message->getCard())
            );
        }
        if ($message instanceof ClueMessage) {
            $messageEntity->setClue($message->getClue()->__toString());
            $messageEntity->setTarget(
                $this->hydratePlayer($message->getTargetPlayer())
            );
        }

        return $messageEntity
            ->setId($message->getId())
            ->setType($message->getType())
            ->setCreatedAt($message->getCreatedAt())
            ->setClass(get_class($message))
            ->setContent($message->getContent(null));
    }
}
