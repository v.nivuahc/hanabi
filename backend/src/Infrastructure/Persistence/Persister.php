<?php

namespace App\Infrastructure\Persistence;

use App\Domain\Model\Game\Game;
use App\Infrastructure\Entity\GameEntity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class Persister
{
    private EntityManagerInterface $objectManager;
    private ObjectRepository $gameRepository;
    private FromDomainConverter $fromDomainConverter;

    /**
     * @param EntityManagerInterface $objectManager
     * @param FromDomainConverter    $fromDomainConverter
     */
    public function __construct(
        EntityManagerInterface $objectManager,
        FromDomainConverter $fromDomainConverter
    ) {
        $this->objectManager = $objectManager;
        $this->fromDomainConverter = $fromDomainConverter;
        $this->gameRepository = $objectManager->getRepository(GameEntity::class);
    }

    /**
     * Persiste le jeu en argument et en retourne l'entité pendante.
     *
     * @param Game $game
     *
     * @return GameEntity
     */
    public function persist(Game $game): GameEntity
    {
        $gameEntity = $this->fromDomainConverter->convertGame($game);

        $this->objectManager->persist($gameEntity);
        $this->objectManager->flush();

        return $gameEntity;
    }

    /**
     * Retourne le jeu dont l'id est en argument,s'il existe.
     *
     * @param int $gameId
     *
     * @return GameEntity|null
     */
    public function retrieve(int $gameId): ?GameEntity
    {
        return $this->gameRepository->find($gameId);
    }
}
