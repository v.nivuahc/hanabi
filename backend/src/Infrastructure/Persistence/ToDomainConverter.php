<?php

namespace App\Infrastructure\Persistence;

use App\Domain\Action\Message\ClueMessage;
use App\Domain\Action\Message\DiscardCardMessage;
use App\Domain\Action\Message\DrawCardMessage;
use App\Domain\Action\Message\PlayCardMessage;
use App\Domain\Collection\Deck;
use App\Domain\Collection\MapOfCardsByColor;
use App\Domain\Collection\MapOfCardsByColorFactory;
use App\Domain\Model\Card\Card;
use App\Domain\Model\Card\CardType;
use App\Domain\Model\Card\Color;
use App\Domain\Model\Card\Value;
use App\Domain\Model\Clue\ColorClue;
use App\Domain\Model\Clue\ValueClue;
use App\Domain\Model\Game\Config\GameConfigFactory;
use App\Domain\Model\Game\Game;
use App\Domain\Model\Game\GameFactory;
use App\Domain\Model\History\ErrorMessage;
use App\Domain\Model\History\InfoMessage;
use App\Domain\Model\History\Message;
use App\Domain\Model\Player;
use App\Infrastructure\Entity\CardEntity;
use App\Infrastructure\Entity\GameEntity;
use App\Infrastructure\Entity\MessageEntity;
use App\Infrastructure\Entity\PlayerEntity;

class ToDomainConverter
{
    private GameFactory $gameFactory;

    public function __construct(GameFactory $gameFactory)
    {
        $this->gameFactory = $gameFactory;
    }

    /**
     * Retourne le pendant de l'entité dans le domaine.
     *
     * @param GameEntity $gameEntity
     *
     * @return Game
     */
    public function convertGame(GameEntity $gameEntity): Game
    {
        $players = [];
        $activePlayer = null;
        foreach ($gameEntity->getPlayers() as $playerEntity) {
            $player = $this->getPlayer($playerEntity);
            $players[] = $player;

            if ($gameEntity->getActivePlayer() === $playerEntity) {
                $activePlayer = $player;
            }
        }

        if ($activePlayer === null) {
            throw new \InvalidArgumentException('La partie n\'a pas de joueur actif');
        }

        $game = $this->gameFactory->loadGame(
            $players,
            GameConfigFactory::initGameConfig($gameEntity->getMode())
        );

        while ($game->getNbOfRemainingClueToken() !== $gameEntity->getNbOfRemainingClueToken()) {
            $game->decrementNbOfRemainingClueToken();
        }

        while ($game->getNbOfRemainingBombsBeforeLose() !== $gameEntity->getNbOfRemainingBombsBeforeLose()) {
            $game->decrementNbOfRemainingBombsBeforeLose();
        }

        $game->setDeck($this->getDeck($gameEntity->getDeck()->toArray()));
        $game->setDiscard(
            $this->getDiscard(
                $gameEntity->getDiscard()->toArray(),
                $game->getConfig()->getColors()
            )
        );
        $game->setPlayedCards(
            $this->getPlayedCards(
                $gameEntity->getPlayedCards()->toArray(),
                $game->getConfig()->getColors()
            )
        );
        $this->setHistory($game, $gameEntity->getHistory()->toArray());
        $game->getPlayers()->setActive($activePlayer);

        return $game
            ->setId($gameEntity->getId())
            ->setStatus($gameEntity->getStatus());
    }

    /**
     * Retourne le pendant comme pioche du domaine.
     *
     * @param CardEntity[]|array $cardEntities
     *
     * @return Deck
     */
    private function getDeck(array $cardEntities): Deck
    {
        $cards = [];
        foreach ($cardEntities as $cardEntity) {
            $cards[] = $this->getCard($cardEntity);
        }

        return new Deck($cards);
    }

    /**
     * Retourne le pendant comme défausse du domaine.
     *
     * @param CardEntity[]|array $cardEntities
     * @param Color[]|array      $colors
     *
     * @return MapOfCardsByColor
     */
    private function getDiscard(array $cardEntities, array $colors): MapOfCardsByColor
    {
        $discard = MapOfCardsByColorFactory::initMapOfSortedCardLists();
        foreach ($colors as $color) {
            $discard->addColor($color);
        }

        foreach ($cardEntities as $cardEntity) {
            $discard->addCard($this->getCard($cardEntity));
        }

        return $discard;
    }

    /**
     * Retourne le pendant comme cartes jouées du domaine.
     *
     * @param CardEntity[]|array $cardEntities
     * @param Color[]|array      $colors
     *
     * @return MapOfCardsByColor
     */
    private function getPlayedCards(array $cardEntities, array $colors): MapOfCardsByColor
    {
        $playedCards = MapOfCardsByColorFactory::initMapOfSuccessiveCardLists();
        foreach ($colors as $color) {
            $playedCards->addColor($color);
        }

        foreach ($cardEntities as $cardEntity) {
            $playedCards->addCard($this->getCard($cardEntity));
        }

        return $playedCards;
    }

    /**
     * Retourne le pendant dans domaine.
     *
     * @param Game                  $game
     * @param MessageEntity[]|array $messageEntities
     */
    private function setHistory(Game $game, array $messageEntities): void
    {
        foreach ($messageEntities as $messageEntity) {
            $game->addMessage(
                $this->getMessage($messageEntity)
            );
        }

    }

    /**
     * Retourne le pendant dans le domaine.
     *
     * @param PlayerEntity $playerEntity
     *
     * @return Player
     */
    private function getPlayer(PlayerEntity $playerEntity): Player
    {
        $player = new Player($playerEntity->getName());

        foreach ($playerEntity->getHand() as $cardEntity) {
            $player->draw($this->getCard($cardEntity));
        }

        return $player->setId($playerEntity->getId());
    }

    /**
     * Retourne le pendant dans le domaine.
     *
     * @param CardEntity $cardEntity
     *
     * @return Card
     */
    private function getCard(CardEntity $cardEntity): Card
    {
        $cardType = new CardType(
            new Value($cardEntity->getValue()),
            new Color($cardEntity->getColor())
        );

        $card = new Card($cardType);
        if ($cardEntity->getColorClue() !== null) {
            $card->setColorClue(new ColorClue(
                new Color($cardEntity->getColorClue()->getColor())
            ));
        }
        if ($cardEntity->getValueClue() !== null) {
            $card->setValueClue(new ValueClue(
                new Value($cardEntity->getValueClue()->getValue())
            ));
        }

        return $card->setId($cardEntity->getId())
            ->setIndex($cardEntity->getIndex());
    }

    /**
     * Retourne le pendant dans le domaine.
     *
     * @param MessageEntity $messageEntity
     *
     * @return Message
     *
     * @throws \InvalidArgumentException
     */
    private function getMessage(MessageEntity $messageEntity): Message
    {
        if (is_a($messageEntity->getClass(), InfoMessage::class, true)) {
            if ($messageEntity->getContent() === null) {
                throw new \InvalidArgumentException(sprintf(
                    'Le message d\'information %s n\'a pas de contenu',
                    $messageEntity->getId()
                ));
            }
            $message = new InfoMessage($messageEntity->getContent());
        } elseif (is_a($messageEntity->getClass(), ErrorMessage::class, true)) {
            if ($messageEntity->getContent() === null) {
                throw new \InvalidArgumentException(sprintf(
                    'Le message d\'information %s n\'a pas de contenu',
                    $messageEntity->getId()
                ));
            }
            $message = new ErrorMessage($messageEntity->getContent());
        } elseif (is_a($messageEntity->getClass(), PlayCardMessage::class, true)) {
            if ($messageEntity->getActor() === null
                || $messageEntity->getCard() === null) {
                throw new \InvalidArgumentException(sprintf(
                    'Le message d\'information %s n\'a pas d\'acteur ou de carte',
                    $messageEntity->getId()
                ));
            }
            $message = new PlayCardMessage(
                $this->getPlayer($messageEntity->getActor()),
                $this->getCard($messageEntity->getCard()),
                $messageEntity->getType() === Message::TYPE_INFO
            );
        } elseif (is_a($messageEntity->getClass(), DiscardCardMessage::class, true)) {
            if ($messageEntity->getActor() === null
                || $messageEntity->getCard() === null) {
                throw new \InvalidArgumentException(sprintf(
                    'Le message d\'information %s n\'a pas d\'acteur ou de carte',
                    $messageEntity->getId()
                ));
            }
            $message = new DiscardCardMessage(
                $this->getPlayer($messageEntity->getActor()),
                $this->getCard($messageEntity->getCard())
            );
        } elseif (is_a($messageEntity->getClass(), DrawCardMessage::class, true)) {
            if ($messageEntity->getActor() === null
                || $messageEntity->getCard() === null) {
                throw new \InvalidArgumentException(sprintf(
                    'Le message d\'information %s n\'a pas d\'acteur ou de carte',
                    $messageEntity->getId()
                ));
            }
            $message = new DrawCardMessage(
                $this->getPlayer($messageEntity->getActor()),
                $this->getCard($messageEntity->getCard())
            );
        } elseif (is_a($messageEntity->getClass(), ClueMessage::class, true)) {
            if ($messageEntity->getActor() === null
                || $messageEntity->getTarget() === null
                || $messageEntity->getClue() === null) {
                throw new \InvalidArgumentException(sprintf(
                    'Le message d\'information %s n\'a pas d\'acteur ou de cible ou d\'indice',
                    $messageEntity->getId()
                ));
            }
            if (is_numeric($messageEntity->getClue())) {
                $clue = new ValueClue(new Value($messageEntity->getClue()));
            } else {
                $clue = new ColorClue(new Color($messageEntity->getClue()));
            }
            $message = new ClueMessage(
                $this->getPlayer($messageEntity->getActor()),
                $clue,
                $this->getPlayer($messageEntity->getTarget())
            );
        } else {
            throw new \InvalidArgumentException(sprintf(
                '"%s" n\'est pas un type de message connu',
                $messageEntity->getClass()
            ));
        }

        return $message
            ->setId($messageEntity->getId())
            ->setCreatedAt($messageEntity->getCreatedAt())
            ->setType($messageEntity->getType());
    }
}
