<?php

declare(strict_types=1);

namespace App\Infrastructure\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200615181300 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE value_clue_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE player_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE game_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE card_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE message_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE color_clue_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE value_clue (id INT NOT NULL, value VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE player (id INT NOT NULL, game_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_98197A65E48FD905 ON player (game_id)');
        $this->addSql('CREATE TABLE players_cards (player_id INT NOT NULL, card_id INT NOT NULL, PRIMARY KEY(player_id, card_id))');
        $this->addSql('CREATE INDEX IDX_19E7715799E6F5DF ON players_cards (player_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_19E771574ACC9A20 ON players_cards (card_id)');
        $this->addSql('CREATE TABLE game (id INT NOT NULL, active_player_id INT DEFAULT NULL, mode VARCHAR(255) NOT NULL, nb_of_remaining_clue_token INT NOT NULL, nb_of_remaining_bombs_before_lose INT NOT NULL, status VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_232B318C8F70B6EB ON game (active_player_id)');
        $this->addSql('CREATE TABLE game_decks_cards (game_id INT NOT NULL, card_id INT NOT NULL, PRIMARY KEY(game_id, card_id))');
        $this->addSql('CREATE INDEX IDX_68C60798E48FD905 ON game_decks_cards (game_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_68C607984ACC9A20 ON game_decks_cards (card_id)');
        $this->addSql('CREATE TABLE game_discards_cards (game_id INT NOT NULL, card_id INT NOT NULL, PRIMARY KEY(game_id, card_id))');
        $this->addSql('CREATE INDEX IDX_DBEDA31AE48FD905 ON game_discards_cards (game_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DBEDA31A4ACC9A20 ON game_discards_cards (card_id)');
        $this->addSql('CREATE TABLE game_played_cards_cards (game_id INT NOT NULL, card_id INT NOT NULL, PRIMARY KEY(game_id, card_id))');
        $this->addSql('CREATE INDEX IDX_5B7A1030E48FD905 ON game_played_cards_cards (game_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5B7A10304ACC9A20 ON game_played_cards_cards (card_id)');
        $this->addSql('CREATE TABLE card (id INT NOT NULL, color_clue_id INT DEFAULT NULL, value_clue_id INT DEFAULT NULL, index INT DEFAULT NULL, color VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_161498D3AF58BFA5 ON card (color_clue_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_161498D3176C47EB ON card (value_clue_id)');
        $this->addSql('CREATE TABLE message (id INT NOT NULL, game_id INT DEFAULT NULL, actor_player_id INT DEFAULT NULL, card_id INT DEFAULT NULL, target_player_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, class VARCHAR(255) NOT NULL, content VARCHAR(255) DEFAULT NULL, clue VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B6BD307FE48FD905 ON message (game_id)');
        $this->addSql('CREATE INDEX IDX_B6BD307F6D9F05F3 ON message (actor_player_id)');
        $this->addSql('CREATE INDEX IDX_B6BD307F4ACC9A20 ON message (card_id)');
        $this->addSql('CREATE INDEX IDX_B6BD307FAD5287F3 ON message (target_player_id)');
        $this->addSql('CREATE TABLE color_clue (id INT NOT NULL, color VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE player ADD CONSTRAINT FK_98197A65E48FD905 FOREIGN KEY (game_id) REFERENCES game (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE players_cards ADD CONSTRAINT FK_19E7715799E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE players_cards ADD CONSTRAINT FK_19E771574ACC9A20 FOREIGN KEY (card_id) REFERENCES card (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C8F70B6EB FOREIGN KEY (active_player_id) REFERENCES player (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE game_decks_cards ADD CONSTRAINT FK_68C60798E48FD905 FOREIGN KEY (game_id) REFERENCES game (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE game_decks_cards ADD CONSTRAINT FK_68C607984ACC9A20 FOREIGN KEY (card_id) REFERENCES card (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE game_discards_cards ADD CONSTRAINT FK_DBEDA31AE48FD905 FOREIGN KEY (game_id) REFERENCES game (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE game_discards_cards ADD CONSTRAINT FK_DBEDA31A4ACC9A20 FOREIGN KEY (card_id) REFERENCES card (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE game_played_cards_cards ADD CONSTRAINT FK_5B7A1030E48FD905 FOREIGN KEY (game_id) REFERENCES game (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE game_played_cards_cards ADD CONSTRAINT FK_5B7A10304ACC9A20 FOREIGN KEY (card_id) REFERENCES card (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE card ADD CONSTRAINT FK_161498D3AF58BFA5 FOREIGN KEY (color_clue_id) REFERENCES color_clue (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE card ADD CONSTRAINT FK_161498D3176C47EB FOREIGN KEY (value_clue_id) REFERENCES value_clue (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FE48FD905 FOREIGN KEY (game_id) REFERENCES game (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F6D9F05F3 FOREIGN KEY (actor_player_id) REFERENCES player (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F4ACC9A20 FOREIGN KEY (card_id) REFERENCES card (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FAD5287F3 FOREIGN KEY (target_player_id) REFERENCES player (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE card DROP CONSTRAINT FK_161498D3176C47EB');
        $this->addSql('ALTER TABLE players_cards DROP CONSTRAINT FK_19E7715799E6F5DF');
        $this->addSql('ALTER TABLE game DROP CONSTRAINT FK_232B318C8F70B6EB');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT FK_B6BD307F6D9F05F3');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT FK_B6BD307FAD5287F3');
        $this->addSql('ALTER TABLE player DROP CONSTRAINT FK_98197A65E48FD905');
        $this->addSql('ALTER TABLE game_decks_cards DROP CONSTRAINT FK_68C60798E48FD905');
        $this->addSql('ALTER TABLE game_discards_cards DROP CONSTRAINT FK_DBEDA31AE48FD905');
        $this->addSql('ALTER TABLE game_played_cards_cards DROP CONSTRAINT FK_5B7A1030E48FD905');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT FK_B6BD307FE48FD905');
        $this->addSql('ALTER TABLE players_cards DROP CONSTRAINT FK_19E771574ACC9A20');
        $this->addSql('ALTER TABLE game_decks_cards DROP CONSTRAINT FK_68C607984ACC9A20');
        $this->addSql('ALTER TABLE game_discards_cards DROP CONSTRAINT FK_DBEDA31A4ACC9A20');
        $this->addSql('ALTER TABLE game_played_cards_cards DROP CONSTRAINT FK_5B7A10304ACC9A20');
        $this->addSql('ALTER TABLE message DROP CONSTRAINT FK_B6BD307F4ACC9A20');
        $this->addSql('ALTER TABLE card DROP CONSTRAINT FK_161498D3AF58BFA5');
        $this->addSql('DROP SEQUENCE value_clue_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE player_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE game_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE card_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE message_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE color_clue_id_seq CASCADE');
        $this->addSql('DROP TABLE value_clue');
        $this->addSql('DROP TABLE player');
        $this->addSql('DROP TABLE players_cards');
        $this->addSql('DROP TABLE game');
        $this->addSql('DROP TABLE game_decks_cards');
        $this->addSql('DROP TABLE game_discards_cards');
        $this->addSql('DROP TABLE game_played_cards_cards');
        $this->addSql('DROP TABLE card');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE color_clue');
    }
}
