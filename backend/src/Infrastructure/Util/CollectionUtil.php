<?php

namespace App\Infrastructure\Util;

use Doctrine\Common\Collections\Collection;

class CollectionUtil
{
    /**
     * Retourne le première entité de la collection dont l'id est celui passé en argument.
     *
     * @param Collection<int, mixed> $collection
     * @param int|null               $entityId
     *
     * @return object|null
     */
    public static function get(Collection $collection, ?int $entityId): ?object
    {
        if ($entityId === null) {
            return null;
        }

        $entities = $collection->toArray();
        $filterById = fn($entity) => $entity->getId() === $entityId;

        return current(array_filter($entities, $filterById)) ?: null;
    }
}
