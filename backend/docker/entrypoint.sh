#!/bin/bash -x

# Ajout du .env.local
if [ ! -f "/srv/app/.env.local" ]; then
    printf "###> symfony/framework-bundle ###\nAPP_ENV=dev\n###< symfony/framework-bundle ###" > /srv/app/.env.local
    printf "\n\n###> hanabi ###\nFRONTEND_BASE_URL=%s\n###< hanabi ###" \
    "http://localhost:4300"
    >> /srv/app/.env.local
fi

# Attente du lancement de la base de données
RETRIES=45
while ! pg_isready -h postgres || [ $RETRIES -eq 0 ]
do
    echo "Waiting for postgres server, $((RETRIES--)) remaining attempts..."
    sleep 1
    if [ $RETRIES -eq 0 ]; then
      echo "We have been waiting for Postgres too long already; failing."
      exit 1
    fi;
done
echo "Database is ready"

# Installation des dépendances
composer install

# Installation de PHPUnit
bin/phpunit --version

# Préparation de la base de données
composer install
php bin/console doctrine:database:drop --if-exists --force
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate --no-interaction

# Lancement du serveur
echo "Lancement du serveur"
php bin/console server:run 0.0.0.0:8000
