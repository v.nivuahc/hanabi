import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Observable, of, Subscription, timer} from 'rxjs';
import {catchError, finalize, mergeMap, take} from 'rxjs/operators';
import {Action} from '../../model/action.model';
import {Card, COLORS} from '../../model/card.model';
import {Game, Mode, Status} from '../../model/game.model';
import {Player} from '../../model/player.model';
import {ActionService} from '../../service/action.service';
import {GameService} from '../../service/game.service';

@Component({
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit, OnDestroy {
  public game: Game = null;
  public player: Player = null;

  public readonly REFRESH_INTERVAL = 10;
  public readonly VALUES = ['1', '2', '3', '4', '5'];
  public COLORS = COLORS;

  public selectedClue: string | null = null;
  public selectedCard: Card | null = null;
  public refreshSubscription: Subscription = null;
  public isGameLoading = false;
  public gameLoadingError: string = null;

  constructor(
    private route: ActivatedRoute,
    private gameService: GameService,
    private actionService: ActionService,
  ) {
  }

  /**
   * @inheritDoc
   */
  ngOnInit(): void {
    this.getGame();
  }

  /**
   * @inheritDoc
   */
  ngOnDestroy(): void {
    if (this.refreshSubscription) {
      this.refreshSubscription.unsubscribe();
    }
  }

  /**
   * Sélectionne l'indice en argument.
   * @param clue
   */
  selectClue(clue: string): void {
    if (!this.canPlay()) {
      this.selectedClue = null;
    } else {
      this.selectedClue = clue;
    }

    this.selectedCard = null;
  }

  /**
   * Sélectionne une carte de sa main.
   * @param card
   */
  selectCard(card: Card): void {
    if (!this.canPlay()) {
      this.selectedCard = null;
    } else {
      this.selectedCard = card;
    }

    this.selectedClue = null;
  }

  /**
   * Retourne le nom court de la carte (initiale de la couleur + valeur concaténées).
   * @param color
   */
  getPlayedCard(color: string): string {
    if (!this.game.playedCards[color]) {
      return '-';
    }

    return `${this.initial(color)}${this.game.playedCards[color]}`;
  }

  /**
   * Retourne l'initiale de la chaîne en argument, en majuscule.
   * @param string
   */
  initial(string: string): string {
    return string[0].toUpperCase();
  }

  /**
   * Retourne le message formatté
   */
  formatMessage(message: string): string {
    message = message
      .replace(new RegExp('{([/]?)clue}', 'g'), '<$1strong>')
      .replace(new RegExp('{([/]?)player}', 'g'), '<$1strong>')
      .replace(new RegExp('{([/]?)card}', 'g'), '<$1strong>')
      .replace(new RegExp('{([/]?)bomb}', 'g'), '<$1strong>')
      .replace(new RegExp('{([/]?)score}', 'g'), '<$1strong>');

    message = message
      .replace(new RegExp('{(info|error)}', 'g'), `<div class="$1">`)
      .replace(new RegExp('{/(info|error)}', 'g'), '</div>');

    for (let color in COLORS) {
      message = message
        .replace(`{${COLORS[color]}}`, `<span class="${COLORS[color]}">`)
        .replace(`{/${COLORS[color]}}`, '</span>');
    }

    return message;
  }

  /**
   * Retourne si le joueur courant peut effectuer des actions.
   */
  canPlay(): boolean {
    return this.game.status === Status.RUNNING
      && this.game.activePlayerIndex === 0
      && !this.hasActionPending();
  }

  /**
   * Retourne si la carte en argument doit être surligné au vue de l'indice sélectionné.
   * @param card
   */
  shouldBeHighlighted(card: { color: string, value: string }): boolean {
    return this.selectedClue === card.color || this.selectedClue === card.value;
  }

  /**
   * Retourne le libellé du mode de jeu en argument.
   * @param mode
   */
  getModeLabel(mode: string) {
    const modeMap = {
      [Mode.NORMAL]: 'normal',
      [Mode.MORE_DIFFICULT]: 'plus difficile',
      [Mode.HARD]: 'difficile',
      [Mode.VERY_HARD]: 'très difficile',
    };

    return modeMap[mode] || 'inconnu';
  }

  /**
   * Retourne la description du mode de jeu en argument.
   * @param mode
   */
  getModeDescription(mode: string) {
    const modeMap = {
      [Mode.NORMAL]: '(pas de cartes multicolores)',
      [Mode.MORE_DIFFICULT]: '(10 cartes multicolores)',
      [Mode.HARD]: '(5 cartes multicolores)',
      [Mode.VERY_HARD]: '(10 cartes multicolores)',
    };

    return modeMap[mode] || null;
  }

  /**
   * Donne l'indice au joueur
   * @param clue
   * @param targetPlayer
   */
  clue(clue: string | null, targetPlayer: Player): void {
    if (clue) {
      this.actionService.execute(Action.CLUE, this.game, this.player.id, {clue, targetPlayer})
        .subscribe((game: Game) => {
          this.loadGame(game);
        });
    }
  }

  /**
   * Joue la carte.
   * @param card
   */
  playCard(card: Card | null): void {
    if (card) {
      this.actionService.execute(Action.PLAY, this.game, this.player.id, {card})
        .subscribe((game: Game) => {
          this.loadGame(game);
        });
    }
  }

  /**
   * Défausse la carte.
   * @param card
   */
  discardCard(card: Card | null): void {
    if (card) {
      this.actionService.execute(Action.DISCARD, this.game, this.player.id, {card})
        .subscribe((game: Game) => {
          this.loadGame(game);
        });
    }
  }

  /**
   * Télécharge la partie.
   */
  private getGame(): void {
    this.isGameLoading = true;
    this.route.paramMap
      .pipe(
        take(1),
        mergeMap<ParamMap, Observable<Game>>((paramMap: ParamMap) => {
          return this.gameService.getGame(
            +paramMap.get('gameId'),
            +paramMap.get('playerId'),
          );
        }),
        catchError((error) => {
          this.gameLoadingError = error.error;

          return of(null);
        }),
        finalize(() => {
          this.isGameLoading = false;
        }),
      )
      .subscribe((game: Game | null) => {
        if (game !== null) {
          this.gameLoadingError = null;
          this.loadGame(game);
        }
      });
  }

  /**
   * Charge la partie.
   */
  private loadGame(game: Game): void {
    this.addCurrentPlayerMessage(game);

    this.game = game;
    this.player = game.players[0];
    this.selectedClue = null;
    this.selectedCard = null;

    this.handleRefreshing(game.activePlayerIndex !== 0);
  }

  /**
   * Gère le rafraîchissement régulier de la partie.
   *
   * Celui-ci est désactivé si le joueur est actif.
   */
  private handleRefreshing(activate: boolean): void {
    if (!activate && this.refreshSubscription !== null) {
      this.refreshSubscription.unsubscribe();
      this.refreshSubscription = null;
      return;
    }

    if (this.refreshSubscription === null) {
      this.refreshSubscription = timer(this.REFRESH_INTERVAL * 1000, this.REFRESH_INTERVAL * 1000)
        .subscribe(() => {
          this.getGame();
        });
    }
  }

  /**
   * Retourne si une action est en cours de réalisation.
   */
  private hasActionPending(): boolean {
    return this.actionService.hasActionPending;
  }

  /**
   * Ajoute un message à l'historique, indiquant à qui c'est le tour.
   */
  private addCurrentPlayerMessage(game: Game): void {
    let currentPlayerName = game.players[game.activePlayerIndex].name;
    if (game.activePlayerIndex === 0) {
      currentPlayerName = 'vous';
    }

    game.history.push(`{info}C'est à {player}${currentPlayerName}{/player} de jouer{/info}`);
  }
}
