import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {Game, Mode, NB_PLAYERS} from '../../model/game.model';
import {GameService} from '../../service/game.service';

@Component({
  templateUrl: './game-init.component.html',
  styleUrls: ['./game-init.component.scss'],
})
export class GameInitComponent {
  public MODE = Mode;
  public NB_PLAYERS = NB_PLAYERS;

  constructor(
    private gameService: GameService,
    private router: Router,
  ) {
  }

  public initNewGame(nbPlayers: number, mode: string): void {
    this.gameService.initNewGame(nbPlayers, mode)
      .subscribe((game: Game) => {
        this.router.navigate(['game', game.id, 'player', game.players[0].id]);
      });
  }
}
