import {Player} from './player.model';

export enum Mode {
  NORMAL = 'normal',
  MORE_DIFFICULT = 'more_difficult',
  HARD = 'hard',
  VERY_HARD = 'very_hard',
}

export enum Status {
  RUNNING = 'running',
  WON = 'won',
  LOST = 'lost',
}

export const NB_PLAYERS = [2, 3, 4, 5];

export class Game {
  id: number;
  mode: Mode;
  colors: string[];
  nbCardsInDeck: number;
  nbClueTokens: number;
  nbClueTokenMax: number;
  nbBombs: number;
  nbBombMax: number;
  showMulti: boolean;
  playedCards: { [color: string]: number };
  discardedCards: { [color: string]: number[] };
  players: Player[];
  activePlayerIndex: number;
  history: string[];
  status: Status;
}
