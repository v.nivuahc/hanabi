export enum Action {
  DISCARD = 'discard',
  PLAY = 'play',
  CLUE = 'clue',
}
