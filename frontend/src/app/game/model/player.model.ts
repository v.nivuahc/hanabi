import {Card} from './card.model';

export class Player {
  id: number;
  name: string;
  hand: Card[];
}
