export enum COLORS {
  BLUE = 'blue',
  GREEN = 'green',
  RED = 'red',
  WHITE = 'white',
  YELLOW = 'yellow',
  MULTI = 'multi',
}

export class Card {
  id: number;
  color: string | null;
  value: string | null;
  clues: {
    color: string | null,
    value: string | null
  };
}
