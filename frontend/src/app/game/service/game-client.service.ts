import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {RouteConfig} from '../../route-config.const';
import {Game} from '../model/game.model';

@Injectable({
  providedIn: 'root',
})
export class GameClientService {
  constructor(
    private httpClient: HttpClient,
  ) {
  }

  /**
   * Récupère une nouvelle partie.
   */
  public initNewGame(nbPlayers: number, mode: string): Observable<Game> {
    return this.httpClient.get<any>(
      RouteConfig.BASE_URL + RouteConfig.GAME_INIT,
      {
        params:
          {
            'nb_players': nbPlayers + '',
            'mode': mode,
          },
      },
    );
  }

  /**
   * Récupère une partie existante, pour un joueur donné.
   */
  public getGame(gameId: number, playerId: number): Observable<Game> {
    const url = RouteConfig.BASE_URL + RouteConfig.GAME_GET;
    return this.httpClient.get<Game>(
      url.replace('{id}', gameId + ''),
      {params: {'player_id': playerId + ''}},
    );
  }
}
