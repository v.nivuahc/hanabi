import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Game} from '../model/game.model';
import {GameClientService} from './game-client.service';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  constructor(
    private client: GameClientService,
  ) {
  }

  /**
   * Récupère une nouvelle partie.
   */
  public initNewGame(nbPlayers: number, mode: string): Observable<Game> {
    return this.client.initNewGame(nbPlayers, mode);
  }

  /**
   * Récupère une partie existante,pour un joueur donné.
   */
  public getGame(gameId: number, playerId: number): Observable<Game> {
    return this.client.getGame(gameId, playerId);
  }
}
