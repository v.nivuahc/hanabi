import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {RouteConfig} from '../../route-config.const';
import {Action} from '../model/action.model';
import {Game} from '../model/game.model';

@Injectable({
  providedIn: 'root',
})
export class ActionClientService {
  constructor(
    private httpClient: HttpClient,
  ) {
  }

  /**
   * Effectue l'action demandée.
   */
  public executeAction(type: Action, game: Game, actorId: number, args: any): Observable<Game> {
    const actionMap = {
      [Action.DISCARD]: () => this.doDiscardAction(game, actorId, args),
      [Action.PLAY]: () => this.doPlayAction(game, actorId, args),
      [Action.CLUE]: () => this.doClueAction(game, actorId, args),
    };

    return actionMap[type]();
  }

  /**
   * Effectue l'action de défausser une carte.
   */
  private doDiscardAction(game: Game, actorId: number, args: any): Observable<Game> {
    const url = RouteConfig.BASE_URL + RouteConfig.ACTION_POST;

    return this.httpClient.post<any>(
      url
        .replace('{id}', game.id + '')
        .replace('{type}', Action.DISCARD)
        .replace('{actorId}', actorId + ''),
      {
        'card': args.card.id,
      },
    );
  }

  /**
   * Effectue l'action de jouer une carte.
   */
  private doPlayAction(game: Game, actorId: number, args: any): Observable<Game> {
    const url = RouteConfig.BASE_URL + RouteConfig.ACTION_POST;

    return this.httpClient.post<any>(
      url
        .replace('{id}', game.id + '')
        .replace('{type}', Action.PLAY)
        .replace('{actorId}', actorId + ''),
      {
        'card': args.card.id,
      },
    );
  }

  /**
   * Effectue l'action de donner un indice.
   */
  private doClueAction(game: Game, actorId: number, args: any): Observable<Game> {
    const url = RouteConfig.BASE_URL + RouteConfig.ACTION_POST;

    return this.httpClient.post<any>(
      url
        .replace('{id}', game.id + '')
        .replace('{type}', Action.CLUE)
        .replace('{actorId}', actorId + ''),
      {
        'clue': args.clue,
        'target_player': args.targetPlayer.id,
      },
    );
  }
}
