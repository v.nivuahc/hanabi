import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {catchError, finalize} from 'rxjs/operators';
import {Action} from '../model/action.model';
import {Game} from '../model/game.model';
import {ActionClientService} from './action-client.service';

@Injectable({
  providedIn: 'root',
})
export class ActionService {
  private actionPending = false;

  constructor(
    private client: ActionClientService,
  ) {
  }

  /**
   * Effectue une action.
   */
  public execute(type: Action, game: Game, actorId: number, args: any): Observable<Game> {
    this.actionPending = true;

    return this.client.executeAction(type, game, actorId, args)
      .pipe(
        catchError((error) => {
          this.addErrorMessage(game, error.error);

          return of(game);
        }),
        finalize(() => {
          this.actionPending = false;
        }),
      );
  }

  /**
   * Retourne si une action est en cours.
   */
  get hasActionPending(): boolean {
    return this.actionPending;
  }

  /**
   * Ajoute un message d'erreur temporaire à l'historique.
   */
  private addErrorMessage(game: Game, message: string): void {
    game.history.push(`{error}${message}{/error}`);
  }
}
