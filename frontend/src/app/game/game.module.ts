import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {GameInitComponent} from './component/game-init/game-init.component';
import {GameComponent} from './component/game/game.component';
import {KeepHtmlPipe} from './pipes/keep-html.pipe';

@NgModule({
  declarations: [
    GameComponent,
    GameInitComponent,
    KeepHtmlPipe,
  ],
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    RouterModule,
  ],
  providers: [],
})
export class GameModule {
}
