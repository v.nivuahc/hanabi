import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {GameInitComponent} from './game/component/game-init/game-init.component';
import {GameModule} from './game/game.module';

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule,
    RouterModule.forRoot([
      {path: '', component: GameInitComponent},
    ]),
    GameModule,
    HttpClientModule,
  ],
  declarations: [
    AppComponent,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
