import {NgModule} from '@angular/core';
import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import {GameInitComponent} from './game/component/game-init/game-init.component';
import {GameComponent} from './game/component/game/game.component';

const APP_ROUTES: Routes = [
  {path: '', redirectTo: '/game/init', pathMatch: 'full'},
  {path: 'game/init', component: GameInitComponent},
  {path: 'game/:gameId/player/:playerId', component: GameComponent},
];

const ROUTER_OPTIONS: ExtraOptions = {
  useHash: false,
  anchorScrolling: 'enabled',
  onSameUrlNavigation: 'reload',
};

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES, ROUTER_OPTIONS)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
