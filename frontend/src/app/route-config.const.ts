export const RouteConfig = {
  BASE_URL: 'http://localhost:8000/',
  GAME_INIT: 'api/game/init',
  GAME_GET: 'api/game/{id}',
  ACTION_POST: 'api/game/{id}/action/{type}/{actorId}',
};
