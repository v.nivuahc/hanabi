#!/usr/bin/env bash

# Options :
#  --push : Lance le push après le build, sans demander confirmation
#  --no-push : S'arrête après le build, sans demander si on veut push

# Color and styles
NORMAL=$'\e[0m'
GREY=$'\e[37m'
BLUE=$'\e[36m'
RED=$'\e[31;01m'
YELLOW=$'\e[33m'
SUCCESS=$'\e[1m\e[42m\e[30m'
ERROR=$'\e[1m\e[41m\e[30m'

# Fonction
function join {
    local IFS="$1";
    shift;
    echo "$*";
}

# Images
IMAGES=(base dev test package)

# Check arguments
if [[ ! " ${IMAGES[@]} " =~ " $1 " ]]; then
    echo ""
    echo "${RED}Invalid arguments!${NORMAL}"
    echo ""
    echo "${BLUE}Please give one of those images as argument: $(join ', ' ${IMAGES[@]})."
    echo "${BLUE}Example: ${NORMAL}./docker/build.sh ${IMAGES[0]}${BLUE}."
    exit 1
fi

FORCE_PUSH=0
if [[ $# -eq 2 && "$2" == "--push" ]]; then
    FORCE_PUSH=1
fi

NO_PUSH=0
if [[ $# -eq 2 && "$2" == "--no-push" ]]; then
    NO_PUSH=1
fi

TARGET_IMAGE="$1:latest"
DOCKERFILE="./docker/images/$1/Dockerfile"
HOST='registry.gitlab.com'
USERNAME='v.nivuahc'
REPO="${HOST}/${USERNAME}/hanabi/frontend"

# Build
echo "${YELLOW} + Building target ${NORMAL}${TARGET_IMAGE}"
echo ""
echo "  $ docker build --no-cache -t ${REPO}/${TARGET_IMAGE} -f ${DOCKERFILE} .${NORMAL}"
docker build --no-cache -t ${REPO}/${TARGET_IMAGE} -f ${DOCKERFILE} .
RESULT=$?
echo ""
if [[ ${RESULT} -eq 0 ]]; then
    echo "${SUCCESS} Build successful ${NORMAL}"
else
    echo "${ERROR} Build failed ${NORMAL}"
    echo ""
    exit ${RESULT}
fi

# Push
if [[ ${NO_PUSH} -eq 1 ]]; then
    exit ${RESULT}
fi

if [[ ${FORCE_PUSH} -eq 0 ]]; then
    echo ""
    echo "${BLUE}Push [${NORMAL}y${BLUE}/${NORMAL}n${BLUE}]?${NORMAL}"
    read answer

    if [[ "$answer" != "${answer#[Yy]}" ]] ;then
        FORCE_PUSH=1
    fi
fi

if [[ ${FORCE_PUSH} -eq 1 ]]; then
    echo ""
    echo "${YELLOW} + Pushing target ${NORMAL}${TARGET_IMAGE}"
    echo ""

    if ! grep -q "${HOST}" ~/.docker/config.json ; then
        echo "  $ docker login -u ${USERNAME} ${HOST}"
        docker login -u ${USERNAME} ${HOST}
    fi
    echo "  $ docker push ${REPO}/${TARGET_IMAGE}"
    docker push ${REPO}/${TARGET_IMAGE}
    RESULT=$?
    echo ""
    if [[ ${RESULT} -eq 0 ]]; then
        echo "${SUCCESS} Push successful ${NORMAL}"
    else
        echo "${ERROR} Push failed ${NORMAL}"
        echo ""
        exit ${RESULT}
    fi
    echo ""
fi
